#include "web_socket.hpp"

#include "response.hpp"

#include <limits>
#include <cassert>

#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QString>
#include <QUrl>
#include <QWebSocket>

namespace json_rpc {

WebSocket::
WebSocket(QUrl const& url)
    : m_webSocket { std::make_unique<QWebSocket>() }
{
    this->connect(m_webSocket.get(), &QWebSocket::connected,
                  this,              &WebSocket::connected);
    this->connect(m_webSocket.get(), &QWebSocket::disconnected,
                  this,              &WebSocket::disconnected);
    this->connect(m_webSocket.get(), &QWebSocket::textMessageReceived,
                  this,              &WebSocket::onTextMessageReceived);
    this->connect(m_webSocket.get(), &QWebSocket::errorOccurred,
                  this,              &WebSocket::onError);

    m_webSocket->open(url.toString());
}

WebSocket::~WebSocket() = default;

void WebSocket::
call(QString const& method, QJsonArray const& params, Callback const& callback)
{
    if (!m_webSocket->isValid()) {
        return;
    }

    auto const id = nextId();

    QJsonObject req;
    req.insert("jsonrpc", "2.0");
    req.insert("id", static_cast<qint64>(id));
    req.insert("method", method);
    req.insert("params", params);

    QJsonDocument doc { req };

    m_callbacks.emplace(id, callback);

    assert(m_webSocket->isValid());
    m_webSocket->sendTextMessage(doc.toJson());
}

bool WebSocket::
isValid() const
{
    return m_webSocket->isValid();
}

void WebSocket::
onError()
{
    Q_EMIT error(m_webSocket->errorString());
}

void WebSocket::
onTextMessageReceived(QString const& message)
{
    auto const& document = QJsonDocument::fromJson(message.toUtf8());
    assert(document.isObject());

    auto const& json = document.object();

    auto const& version = json.value("jsonrpc");
    assert(version.isString());
    assert(version.toString() == "2.0");

    if (json.contains("method")) {

        Q_EMIT notification(json["method"].toString(), json["params"]);
    }
    else {
        handleResponse(json);
    }
}

Id WebSocket::
nextId()
{
    auto const result =
        m_serial == std::numeric_limits<Id>::max() ? 0 : ++m_serial;

    return result;
}

static Id
getId(QJsonValue const& value)
{
    assert(value.isDouble());
    auto const id = static_cast<Id>(value.toDouble());

    return id;
}

void WebSocket::
handleResponse(QJsonObject const& res)
{
    assert(res.contains("id"));
    auto const id = getId(res.value("id"));

    auto const it = m_callbacks.find(id);
    assert(it != m_callbacks.end());

    auto const callback = it->second;

    m_callbacks.erase(id);

    callback(Response { res });
}

} // namespace json_rpc
