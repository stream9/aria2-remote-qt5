#ifndef ARIA2_REMOTE_UTILITY_POINTER_HPP
#define ARIA2_REMOTE_UTILITY_POINTER_HPP

#include "qmanaged_ptr.hpp"

template<typename T>
T&
to_ref(T* const ptr)
{
    assert(ptr);
    return *ptr;
}

#endif // ARIA2_REMOTE_UTILITY_POINTER_HPP
