#include "format.hpp"

#include <cassert>

#include <QString>

namespace aria2_remote::util {

QString
formatTime(uint64_t const sec)
{
    if (sec < 60) {
        static QString const fmt { "%1s" };
        return fmt.arg(sec);
    }
    else if (sec < 60 * 60) {
        static QString const fmt { "%1m%2s" };
        return fmt.arg(sec / 60).arg(sec % 60);
    }
    else if (sec < 24 * 60 * 60) {
        static QString const fmt { "%1h%2m%3s" };
        return fmt.arg(sec / 60 / 60).arg(sec / 60 % 60).arg(sec % 60);
    }
    else {
        static QString const fmt { "%1d %2h%3m%4s" };
        return fmt.arg(sec / 24 / 60 / 60)
                  .arg(sec / 60 / 60 % 60)
                  .arg(sec / 60 % 60)
                  .arg(sec % 60);
    }
    assert(false);
}

QString
formatBandwidth(uint64_t const bytePerSec)
{
    if (bytePerSec < 1024) {
        static QString const fmt { "%1 Bps" };
        return fmt.arg(bytePerSec);
    }
    else if (bytePerSec < 1024 * 1024) {
        static QString const fmt { "%1 KiBps" };
        return fmt.arg(static_cast<double>(bytePerSec) / 1024, 0, 'f', 2);
    }
    else if (bytePerSec < 1024 * 1024 * 1024) {
        static QString const fmt { "%1 MiBps" };
        return fmt.arg(static_cast<double>(bytePerSec) / 1024 / 1024, 0, 'f', 2);
    }
    else {
        static QString const fmt { "%1 GiBps" };
        return fmt.arg(static_cast<double>(bytePerSec) / 1024 / 1024 / 1024, 0, 'f', 2);
    }
    assert(false);
}

QString
formatSize(uint64_t const byte)
{
    if (byte < 1024) {
        static QString const fmt { "%1 B" };
        return fmt.arg(byte);
    }
    else if (byte < 1024 * 1024) {
        static QString const fmt { "%1 KiB" };
        return fmt.arg(static_cast<double>(byte) / 1024, 0, 'f', 2);
    }
    else if (byte < 1024 * 1024 * 1024) {
        static QString const fmt { "%1 MiB" };
        return fmt.arg(static_cast<double>(byte) / 1024 / 1024, 0, 'f', 2);
    }
    else {
        static QString const fmt { "%1 GiB" };
        return fmt.arg(static_cast<double>(byte) / 1024 / 1024 / 1024, 0, 'f', 2);
    }
    assert(false);
}

} // namespace aria2_remote::util
