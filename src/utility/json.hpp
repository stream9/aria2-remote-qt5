#ifndef ARIA2_REMOTE_JSON_UTIL_HPP
#define ARIA2_REMOTE_JSON_UTIL_HPP

#include <cstdint>

#include <QJsonDocument>

class QJsonArray;
class QJsonObject;
class QJsonValue;
class QString;

namespace aria2_remote::util {

bool toBool(QJsonValue const&);
uint16_t toUInt16(QJsonValue const&);
int16_t toInt16(QJsonValue const&);
int32_t toInt32(QJsonValue const&);
int64_t toInt64(QJsonValue const&);
QString toString(QJsonValue const&);
QJsonObject toObject(QJsonValue const&);
QJsonArray toArray(QJsonValue const&);

template<typename T>
T convert(QJsonValue const&);

template<> bool convert(QJsonValue const&);
template<> int32_t convert(QJsonValue const&);
template<> int64_t convert(QJsonValue const&);
template<> QString convert(QJsonValue const&);
template<> QJsonObject convert(QJsonValue const&);
template<> QJsonArray convert(QJsonValue const&);

QString toJson(QJsonValue const&,
               QJsonDocument::JsonFormat = QJsonDocument::Compact);

} // namespace aria2_remote::util

#endif // ARIA2_REMOTE_JSON_UTIL_HPP
