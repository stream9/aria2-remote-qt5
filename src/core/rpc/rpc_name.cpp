#include "rpc_name.hpp"

#include <QString>

namespace aria2_remote::rpc_name {

QString
addUri()
{
    return QStringLiteral("aria2.addUri");
}

QString
addTorrent()
{
    return QStringLiteral("aria2.addTorrent");
}

QString
addMetalink()
{
    return QStringLiteral("aria2.addMetalink");
}

QString
remove()
{
    return QStringLiteral("aria2.remove");
}

QString
forceRemove()
{
    return QStringLiteral("aria2.forceRemove");
}

QString
pause()
{
    return QStringLiteral("aria2.pause");
}

QString
pauseAll()
{
    return QStringLiteral("aria2.pauseAll");
}

QString
forcePause()
{
    return QStringLiteral("aria2.forcePause");
}

QString
forcePauseAll()
{
    return QStringLiteral("aria2.forcePauseAll");
}

QString
unpause()
{
    return QStringLiteral("aria2.unpause");
}

QString
unpauseAll()
{
    return QStringLiteral("aria2.unpauseAll");
}

QString
tellActive()
{
    return QStringLiteral("aria2.tellActive");
}

QString
tellWaiting()
{
    return QStringLiteral("aria2.tellWaiting");
}

QString
tellStopped()
{
    return QStringLiteral("aria2.tellStopped");
}

QString
getUris()
{
    return QStringLiteral("aria2.getUris");
}

QString
getPeers()
{
    return QStringLiteral("aria2.getPeers");
}

QString
getServers()
{
    return QStringLiteral("aria2.getServers");
}

QString
changePosition()
{
    return QStringLiteral("aria2.changePosition");
}

QString
getOption()
{
    return QStringLiteral("aria2.getOption");
}

QString
changeOption()
{
    return QStringLiteral("aria2.changeOption");
}

QString
getGlobalOption()
{
    return QStringLiteral("aria2.getGlobalOption");
}

QString
changeGlobalOption()
{
    return QStringLiteral("aria2.changeGlobalOption");
}

QString
getGlobalStat()
{
    return QStringLiteral("aria2.getGlobalStat");
}

QString
removeDownloadResult()
{
    return QStringLiteral("aria2.removeDownloadResult");
}

QString
multiCall()
{
    return QStringLiteral("system.multicall");
}

} // namespace aria2_remote::rpc_name
