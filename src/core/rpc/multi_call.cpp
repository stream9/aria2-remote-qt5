#include "multi_call.hpp"

#include "json_rpc/response.hpp"
#include "utility/json.hpp"
#include "rpc_name.hpp"

#include <cassert>

#include <QDebug>
#include <QJsonDocument>

namespace aria2_remote {

using util::toArray;
using util::toObject;

MultiCall::
MultiCall(Aria2Rpc& aria2)
    : m_aria2 { aria2 }
{}

bool MultiCall::
empty() const
{
    return m_methods.empty();
}

QString MultiCall::
toJson() const
{
    return QJsonDocument(m_methods).toJson();
}

void MultiCall::
exec(Callback const& cb)
{
    auto const& method = rpc_name::multiCall();

    assert(!m_methods.empty());

    QJsonArray params;
    params.push_back(m_methods);

    m_aria2.callAria2(method, params, cb);
}

MultiCall& MultiCall::
tellActive()
{
    auto const& method = rpc_name::tellActive();

    pushMethod(method);

    return *this;
}

MultiCall& MultiCall::
tellWaiting(int const offset, int const num)
{
    auto const& method = rpc_name::tellWaiting();

    pushMethod(method, offset, num);

    return *this;
}

MultiCall& MultiCall::
tellStopped(int const offset, int const num)
{
    auto const& method = rpc_name::tellStopped();

    pushMethod(method, offset, num);

    return *this;
}

MultiCall& MultiCall::
getGlobalOption()
{
    auto const& method = rpc_name::getGlobalOption();

    pushMethod(method);

    return *this;
}

MultiCall& MultiCall::
getGlobalStat()
{
    auto const& method = rpc_name::getGlobalStat();

    pushMethod(method);

    return *this;
}

#if 0
MultiCall& MultiCall::
remove(QString const& gid)
{
    auto const& method = rpc_name::remove();

    pushMethod(method, gid);

    return *this;
}
#endif

QJsonObject MultiCall::
makeMethod(QString const& name, QJsonArray const& params)
{
    QJsonObject method;
    method.insert("methodName", name);
    method.insert("params", params);

    return method;
}

MultiCall::Results MultiCall::
unpackResult(QJsonValue const& value)
{
    Results results;

    auto const& array = toArray(value);

    for (auto const& element: array) {
        if (element.isObject()) {
            results.push_back(json_rpc::Response::fromError(element));
        }
        else {
            auto const& res = toArray(element);
            assert(res.size() == 1);

            results.push_back(json_rpc::Response::fromValue(res[0]));
        }
    }

    return results;
}

} // namespace aria2_remote
