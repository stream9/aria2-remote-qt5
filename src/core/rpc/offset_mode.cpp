#include "offset_mode.hpp"

#include <cassert>

#include <QDebug>
#include <QString>

namespace aria2_remote {

OffsetMode::
OffsetMode(Value const value)
    : m_value { value }
{}

QString const& OffsetMode::
rpcString() const
{
    static QString const posSet = "POS_SET";
    static QString const posCur = "POS_CUR";
    static QString const posEnd = "POS_END";
    static QString const error = "";

    switch (m_value) {
    case Set:
        return posSet;
    case Current:
        return posCur;
    case End:
        return posEnd;
    default:
        qDebug() << "OffsetMode::rpcString(): unknown value" << m_value;
        return error;
    }
}

} // namespace aria2_remote
