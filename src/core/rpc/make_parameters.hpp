#ifndef ARIA2_REMOTE_MAKE_PARAMETERS_HPP
#define ARIA2_REMOTE_MAKE_PARAMETERS_HPP

#include <QJsonArray>

namespace aria2_remote {

inline void
pushParameters(QJsonArray&/*params*/) {}

template<typename T, typename... Args>
void
pushParameters(QJsonArray& params, T&& arg, Args&&... args)
{
    params.push_back(std::forward<T>(arg));

    pushParameters(params, std::forward<Args>(args)...);
}

template<typename... Args>
QJsonArray
makeParameters(Args&&... args)
{
    QJsonArray result;

    pushParameters(result, std::forward<Args>(args)...);

    return result;
}

} // namespace aria2_remote

#endif // ARIA2_REMOTE_MAKE_PARAMETERS_HPP
