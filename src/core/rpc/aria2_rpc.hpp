#ifndef ARIA2_REMOTE_ARIA2_RPC_HPP
#define ARIA2_REMOTE_ARIA2_RPC_HPP

#include "offset_mode.hpp"

#include "json_rpc/types.hpp"

#include <cstdint>
#include <exception>
#include <memory>

#include <QObject>

class QByteArray;
class QJsonObject;
class QJsonValue;
class QUrl;

namespace aria2_remote {

class MultiCall;

class Aria2Rpc : public QObject
{
    Q_OBJECT
public:
    using Callback = json_rpc::Callback;
    using Response = json_rpc::Response;

public:
    Aria2Rpc(QUrl const&);
    ~Aria2Rpc();

    bool isValid() const;

    void addUri(QJsonArray const& uris, Callback const&);
    void addUri(QJsonArray const& uris,
                QJsonObject const& options, Callback const&);
    void addUri(QJsonArray const& uris,
                QJsonObject const& options, int32_t position, Callback const&);

    void addTorrent(QString const& torrent, Callback const&);
    void addTorrent(QString const& torrent,
                    QJsonArray const& uris, Callback const&);
    void addTorrent(QString const& torrent,
                    QJsonArray const& uris,
                    QJsonObject const& options, Callback const&);
    void addTorrent(QString const& torrent,
                    QJsonArray const& uris,
                    QJsonObject const& options,
                    int32_t position, Callback const&);

    void addMetalink(QString const& metalink, Callback const&);
    void addMetalink(QString const& metalink,
                     QJsonObject const& options, Callback const&);
    void addMetalink(QString const& metalink,
                QJsonObject const& options, int32_t position, Callback const&);

    void remove(QString const& gid, Callback const&);
    void forceRemove(QString const& gid, Callback const&);

    void pause(QString const& gid, Callback const&);
    void pauseAll(Callback const&);
    void forcePause(QString const& gid, Callback const&);
    void forcePauseAll(Callback const&);

    void unpause(QString const& gid, Callback const&);
    void unpauseAll(Callback const&);

    void tellActive(Callback const&);
    void tellWaiting(int32_t offset, int32_t num, Callback const&);
    void tellStopped(int32_t offset, int32_t num, Callback const&);

    void getUris(QString const& gid, Callback const&);
    void getPeers(QString const& gid, Callback const&);
    void getServers(QString const& gid, Callback const&);

    void changePosition(QString const& gid, int32_t pos, OffsetMode, Callback const&);

    void getOption(QString const& gid, Callback const&);
    void changeOption(QString const& gid,
                      QJsonObject const& options, Callback const&);

    void getGlobalOption(Callback const&);
    void changeGlobalOption(QJsonObject const& options, Callback const&);

    void getGlobalStat(Callback const&);

    void removeDownloadResult(QString const& gid, Callback const&);

    MultiCall multiCall();

public:
    Q_SIGNAL void connected() const;
    Q_SIGNAL void disconnected() const;
    Q_SIGNAL void error(QString const&) const;
    Q_SIGNAL void notification(QString const& method, QJsonValue const& params);

    Q_SIGNAL void onDownloadStart(QString const& gid);
    Q_SIGNAL void onDownloadPause(QString const& gid);
    Q_SIGNAL void onDownloadStop(QString const& gid);
    Q_SIGNAL void onDownloadComplete(QString const& gid);
    Q_SIGNAL void onDownloadError(QString const& gid);
    Q_SIGNAL void onBtDownloadComplete(QString const& gid);

private:
    Q_SLOT void onNotification(QString const& method, QJsonValue const& params);

    void callAria2(QString const& method, QJsonArray const& params,
                   Callback const&);
private:
    friend class MultiCall;

    std::unique_ptr<json_rpc::WebSocket> m_jsonRpc; // non-null
};

struct Aria2Error : public std::exception
{
    int code;
    QString message;

    Aria2Error() = default;
    Aria2Error(QJsonValue const&);

    Aria2Error(Aria2Error const&) = default;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_ARIA2_RPC_HPP
