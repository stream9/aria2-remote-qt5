#ifndef ARIA2_REMOTE_SERVER_HPP
#define ARIA2_REMOTE_SERVER_HPP

#include <memory>

#include <boost/ptr_container/ptr_vector.hpp>

#include <QObject>
#include <QString>

namespace aria2_remote {

class Application;
class Aria2Remote;

// Server

class Server : public QObject
{
    Q_OBJECT
public:
    Server(QString const& name,
           QString const& host,
           uint16_t port,
           QString const& secret);

    ~Server() override;

    // accessor
    QString const& name() const { return m_name; }
    QString const& host() const { return m_host; }
    QString const& secret() const { return m_secret; }
    uint16_t port() const { return m_port; }
    Aria2Remote* aria2() const;

    // query
    QUrl url() const;

    // modifier
    void assign(QString const& name,
                QString const& host,
                uint16_t port,
                QString const& secret);

    // command
    void connect();
    void disconnect();
    bool isConnected() const;

    Q_SIGNAL void connected() const;
    Q_SIGNAL void disconnected() const;
    Q_SIGNAL void edited() const;
    Q_SIGNAL void connectionError(QString const& msg) const;

    using QObject::connect;
    using QObject::disconnect;

private:
    QString m_name;
    QString m_host;
    QString m_secret;
    uint16_t m_port;
    std::shared_ptr<Aria2Remote> m_aria2;
};

// Servers

class Servers : public QObject
{
    Q_OBJECT
public:
    Servers(Application&);
    ~Servers() override;

    // accessor
    Server& operator[](size_t index) { return m_servers.at(index); }
    Server const& operator[](size_t index) const { return m_servers.at(index); }

    // query
    size_t size() const  { return m_servers.size(); }
    bool empty() const { return m_servers.empty(); }

    auto begin() const { return m_servers.begin(); }
    auto end() const { return m_servers.end(); }

    Server const* find(QString const& name) const;
    Server* find(QString const& name);

    // modifier
    void append(QString const& name,
                QString const& host,
                uint16_t port,
                QString const& secret);

    void remove(Server const&);

    // signals
    Q_SIGNAL void appended() const;
    Q_SIGNAL void removed(size_t index) const;
    Q_SIGNAL void edited(size_t index) const;
    Q_SIGNAL void connectionError(Server&, QString const& msg) const;

private:
    void load();
    void save() const;

    Q_SLOT void onEdited() const;
    Q_SLOT void onConnectionError(QString const& msg) const;

private:
    Application& m_application;
    boost::ptr_vector<Server> m_servers;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_SERVER_HPP
