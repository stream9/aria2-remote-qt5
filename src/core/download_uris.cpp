#include "download_uris.hpp"

#include "aria2_remote.hpp"
#include "download_handle.hpp"
#include "json_rpc/response.hpp"
#include "utility/json.hpp"
#include "rpc/aria2_rpc.hpp"

#include <cassert>

#include <boost/numeric/conversion/cast.hpp>

#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>

namespace aria2_remote {

using util::toArray;
using util::toObject;

DownloadUris::
DownloadUris(DownloadHandle& handle)
    : m_handle { handle }
{
}

void DownloadUris::
update()
{
    auto& rpc = m_handle.aria2().rpc();
    auto const& gid = m_handle.gid();

    rpc.getUris(gid,
        [this, handle = m_handle.shared_from_this()]
        (auto const& res) {
            if (res) {
                this->setUris(res.value());
            }
            else {
                this->clear();
            }
        }
    );
}

void DownloadUris::
setUris(QJsonValue const& res)
{
    auto const& uris = toArray(res);

    bool changed = false;

    if (boost::numeric_cast<size_t>(uris.size()) == m_uris.size()) {
        for (qsizetype idx = 0, len = uris.size(); idx < len; ++idx) {
            auto const& file = toObject(uris[idx]);

            changed |= m_uris[static_cast<size_t>(idx)].update(file);
        }
    }
    else {
        m_uris.clear();
        for (auto const& element: uris) {
            auto const& uri = toObject(element);

            m_uris.emplace_back(uri);
        }
        changed = true;
    }

    if (changed) {
        Q_EMIT this->changed();
    }
}

void DownloadUris::
clear()
{
    if (m_uris.empty()) return;

    m_uris.clear();
    Q_EMIT this->changed();
}

} // namespace aria2_remote
