#include "aria2_remote.hpp"

#include "download_handle.hpp"
#include "download_handles.hpp"
#include "global_options.hpp"
#include "global_status.hpp"
#include "json_rpc/response.hpp"
#include "rpc/aria2_rpc.hpp"
#include "rpc/multi_call.hpp"
#include "server.hpp"

#include "utility/json.hpp"

#include <algorithm>
#include <cassert>

#include <boost/numeric/conversion/cast.hpp>

#include <QDebug>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QString>
#include <QUrl>

namespace aria2_remote {

using util::toArray;
using util::toObject;
using util::toString;

namespace rng = std::ranges;

Aria2Remote::
Aria2Remote(Server const& server)
    : m_server { server }
    , m_aria2 { std::make_unique<Aria2Rpc>(server.url()) }
{
    assert(m_aria2);

    m_globalStatus = std::make_unique<GlobalStatus>(*m_aria2);
    assert(m_globalStatus);

    m_activeHandles = std::make_unique<ActiveHandles>(*this);
    assert(m_activeHandles);

    m_waitingHandles = std::make_unique<WaitingHandles>(*this);
    assert(m_waitingHandles);

    m_stoppedHandles = std::make_unique<StoppedHandles>(*this);
    assert(m_stoppedHandles);

    m_globalOptions = std::make_unique<GlobalOptions>(*this);
    assert(m_globalOptions);

    this->connect(m_aria2.get(), &Aria2Rpc::connected,
                  this,          &Aria2Remote::activate);
    this->connect(m_aria2.get(), &Aria2Rpc::disconnected,
                  this,          &Aria2Remote::deactivate);
    this->connect(m_aria2.get(), &Aria2Rpc::error,
                  this,          &Aria2Remote::error);

    this->connect(m_globalStatus.get(), &GlobalStatus::changed,
                  this,                 &Aria2Remote::onGlobalStatusChanged);

    this->connect(m_activeHandles.get(), &DownloadHandles::changed,
                  this,                  &Aria2Remote::activeDownloadsChanged);

    this->connect(m_waitingHandles.get(), &DownloadHandles::changed,
                  this,                   &Aria2Remote::waitingDownloadsChanged);

    this->connect(m_stoppedHandles.get(), &DownloadHandles::changed,
                  this,                   &Aria2Remote::stoppedDownloadsChanged);

    this->connect(&m_timer, &QTimer::timeout,
                  this,     &Aria2Remote::doRefresh);
}

Aria2Remote::~Aria2Remote() = default;

bool Aria2Remote::
isActive() const
{
    return m_timer.isActive();
}

GlobalStatus const& Aria2Remote::
globalStatus() const
{
    return *m_globalStatus;
}

DownloadHandle* Aria2Remote::
downloadHandle(QString const& gid) const
{
    auto getGid = [](auto&& handle) { return handle->gid(); };

    auto it = rng::find(*m_activeHandles, gid, getGid);
    if (it != m_activeHandles->end()) {
        return it->get();
    }

    it = rng::find(*m_waitingHandles, gid, getGid);
    if (it != m_waitingHandles->end()) {
        return it->get();
    }

    it = rng::find(*m_stoppedHandles, gid, getGid);
    if (it != m_stoppedHandles->end()) {
        return it->get();
    }

    return nullptr;
}

static bool
isError(json_rpc::Response const& res, char const* const location)
{
    if (res) {
        return false;
    }
    else {
        qDebug() << "Error at" << location << res.error();
        return true;
    }
}

void Aria2Remote::
refresh()
{
    doRefresh();
    m_timer.start();
}

void Aria2Remote::
initialize()
{
    m_aria2->multiCall()
        .getGlobalStat()
        .tellActive()
        .tellWaiting(0, 100)
        .tellStopped(0, 100)
        .getGlobalOption()
        .exec([this](auto const& res) {
            if (res.isError()) {
                qDebug() << "Aria2Remote::initialize" << res.error();
                return;
            }

            auto const& results = MultiCall::unpackResult(res.value());
            assert(results.size() == 5);

            if (!isError(results[0], "Aria2Remote::initialize::getGlobalStat")) {
                m_globalStatus->update(toObject(results[0].value()));
            }
            if (!isError(results[1], "Aria2Remote::initialize::tellActive")) {
                m_activeHandles->update(results[1].value());
            }
            if (!isError(results[2], "Aria2Remote::initialize::tellWaiting")) {
                m_waitingHandles->update(results[2].value());
            }
            if (!isError(results[3], "Aria2Remote::initialize::tellStopped")) {
                m_stoppedHandles->update(results[3].value());
            }
            if (!isError(results[4], "Aria2Remote::initialize::getGlobalOptions")) {
                m_globalOptions->update(toObject(results[4].value()));
            }

            Q_EMIT this->initialized();
        }
    );
}

void Aria2Remote::
activate()
{
    m_timer.start(2000);

    initialize();

    Q_SIGNAL activated();
}

void Aria2Remote::
deactivate()
{
    m_timer.stop();

    m_globalStatus->clear();
    m_activeHandles->clear();
    m_waitingHandles->clear();
    m_stoppedHandles->clear();

    Q_SIGNAL deactivated();
}

void Aria2Remote::
doRefresh()
{
    m_globalStatus->update();
    m_activeHandles->refresh();
}

void Aria2Remote::
onGlobalStatusChanged()
{
    if (boost::numeric_cast<size_t>(m_globalStatus->numWaiting())
                                            != m_waitingHandles->size())
    {
        m_waitingHandles->refresh();
    }
    if (boost::numeric_cast<size_t>(m_globalStatus->numStopped())
                                            != m_stoppedHandles->size())
    {
        m_stoppedHandles->refresh();
    }

    Q_EMIT globalStatusChanged();
}

} // namespace aria2_remote
