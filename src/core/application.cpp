#include "application.hpp"

#include "aria2_remote.hpp"
#include "download_handle.hpp"
#include "error_handler.hpp"
#include "notification.hpp"
#include "server.hpp"

#include "gui/action_manager.hpp"
#include "gui/main_window.hpp"

#include <QApplication>
#include <QDir>
#include <QStandardPaths>
#include <QDebug>

namespace aria2_remote {

//
// Application
//
Application::
Application()
    : m_errorHandler { std::make_unique<ErrorHandler>() }
    , m_servers { std::make_unique<Servers>(*this) }
    , m_actionManager { std::make_unique<ActionManager>(*this) }
    , m_notification { std::make_unique<Notification>(*this) }
{
    assert(m_servers);
    assert(m_errorHandler);
    assert(m_actionManager);

    this->connect(m_servers.get(),      &Servers::connectionError,
                  m_errorHandler.get(), &ErrorHandler::onConnectionError);
}

Application::~Application() = default;

QString Application::
applicationName() const
{
    return QCoreApplication::applicationName();
}

MainWindow& Application::
mainWindow() const
{
    if (m_mainWindow) {
        return *m_mainWindow;
    }
    else {
        auto* const widget = QApplication::activeWindow();
        assert(widget);
        return dynamic_cast<MainWindow&>(*widget);
    }
}

void Application::
connectToServer(QString const& name)
{
    auto* const server = m_servers->find(name);
    if (server) {
        server->connect();
    }
}

void Application::
setAria2(Aria2Remote& aria2)
{
    if (m_aria2) {
        m_aria2->disconnect(this);
    }

    m_aria2 = &aria2;

    this->connect(m_aria2, &QObject::destroyed,
                  this,    &Application::clearAria2);

    Q_EMIT aria2Changed(m_aria2);
}

void Application::
clearAria2()
{
    if (m_aria2) {
        m_aria2->disconnect(this);
    }

    m_aria2 = nullptr;

    Q_EMIT aria2Changed(nullptr);
}

void Application::
setCurrentHandle(DownloadHandle& handle)
{
    if (m_currentHandle) {
        m_currentHandle->disconnect(this);
    }

    m_currentHandle = &handle;

    Q_EMIT currentHandleChanged(m_currentHandle);

    this->connect(m_currentHandle, &QObject::destroyed,
                  this,            &Application::clearCurrentHandle);
}

void Application::
clearCurrentHandle()
{
    if (m_currentHandle) {
        m_currentHandle->disconnect(this);
    }

    m_currentHandle = nullptr;

    Q_EMIT currentHandleChanged(nullptr);
}

void Application::
setCurrentServer(Server& currentServer)
{
    if (m_currentServer) {
        m_currentServer->disconnect(this);
    }

    m_currentServer = &currentServer;

    updateAria2();

    if (m_currentServer) {
        this->connect(m_currentServer, &QObject::destroyed,
                      this,            &Application::clearCurrentServer);
        this->connect(m_currentServer, &Server::connected,
                      this,            &Application::updateAria2);
        this->connect(m_currentServer, &Server::disconnected,
                      this,            &Application::updateAria2);

    }

    Q_EMIT currentServerChanged(m_currentServer);
}

void Application::
clearCurrentServer()
{
    if (m_currentServer) {
        m_currentServer->disconnect(this);
    }

    m_currentServer = nullptr;

    Q_EMIT currentServerChanged(nullptr);
}

void Application::
setSelection(Selection&& handles)
{
    m_selection = std::move(handles);

    Q_EMIT selectionChanged();
}

void Application::
clearSelection()
{
    m_selection.clear();

    Q_EMIT selectionChanged();
}

QDir Application::
configDir()
{
    auto const& path =
        QStandardPaths::writableLocation(QStandardPaths::ConfigLocation);
    if (path.isEmpty()) {
        qCritical() << "Can't find path for configuration file.";
        assert(false);
    }

    QDir dir { QDir::cleanPath(path + QDir::separator() + "aria2-remote") };
    if (!dir.exists() && !dir.mkpath(".")) {
        qCritical() << "Can't create config directory.";
        assert(false);
    }

    return dir;
}

void Application::
updateAria2()
{
    auto* const aria2 = m_currentServer->aria2();
    if (aria2) {
        this->setAria2(*aria2);
    }
    else {
        this->clearAria2();
    }
}

Application&
gApp()
{
    static Application app;
    return app;
}

} // namespace aria2_remote
