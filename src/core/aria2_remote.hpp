#ifndef ARIA2_REMOTE_ARIA2_REMOTE_HPP
#define ARIA2_REMOTE_ARIA2_REMOTE_HPP

#include <memory>

#include <QObject>
#include <QTimer>

namespace aria2_remote {

class Aria2Rpc;
class DownloadHandle;
class DownloadHandles;
class GlobalStatus;
class Notification;
class Options;
class Server;

class Aria2Remote : public QObject
                  , public std::enable_shared_from_this<Aria2Remote>
{
    Q_OBJECT
public:
    Aria2Remote(Server const&);
    ~Aria2Remote();

    // accessor
    Server const& server() const { return m_server; }

    GlobalStatus const& globalStatus() const;

    DownloadHandle* downloadHandle(QString const& gid) const;

    DownloadHandles& activeDownloads() { return *m_activeHandles; }
    DownloadHandles& waitingDownloads() { return *m_waitingHandles; }
    DownloadHandles& stoppedDownloads() { return *m_stoppedHandles; }

    Options& globalOptions() { return *m_globalOptions; }

    bool isActive() const;

    Aria2Rpc& rpc() { return *m_aria2; }

    void refresh();

public:
    Q_SIGNAL void activated() const;
    Q_SIGNAL void deactivated() const;
    Q_SIGNAL void initialized() const;
    Q_SIGNAL void error(QString const&) const;

    Q_SIGNAL void globalStatusChanged() const;
    Q_SIGNAL void activeDownloadsChanged() const;
    Q_SIGNAL void waitingDownloadsChanged() const;
    Q_SIGNAL void stoppedDownloadsChanged() const;

private:
    void initialize();

    Q_SLOT void activate();
    Q_SLOT void deactivate();
    Q_SLOT void doRefresh();
    Q_SLOT void onGlobalStatusChanged();

private:
    Server const& m_server;

    std::unique_ptr<GlobalStatus> m_globalStatus; // non-null
    std::unique_ptr<DownloadHandles> m_activeHandles; // non-null
    std::unique_ptr<DownloadHandles> m_waitingHandles; // non-null
    std::unique_ptr<DownloadHandles> m_stoppedHandles; // non-null
    std::unique_ptr<Options> m_globalOptions; // non-null

    QTimer m_timer;

    // We must destruct this last, otherwise cleaning up of other member will fail.
    std::unique_ptr<Aria2Rpc> m_aria2; // non-null
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_ARIA2_REMOTE_HPP
