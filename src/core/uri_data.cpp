#include "uri_data.hpp"

#include "utility/json.hpp"

#include <cassert>

#include <QJsonObject>
#include <QJsonValue>

#include <QDebug>

namespace aria2_remote {

using util::toString;

template<typename T>
static bool
updateValue(T const& src, T& dest)
{
    if (src != dest) {
        dest = src;
        return true;
    }
    return false;
}

// UriData

UriData::
UriData(QJsonObject const& obj)
{
    update(obj);
}

QString const& UriData::
statusText() const
{
    static QString const used { "Used" };
    static QString const waiting { "Waiting" };

    if (m_status == URI_USED) {
        return used;
    }
    else {
        return waiting;
    }
}

bool UriData::
update(QJsonObject const& obj)
{
    bool changed = false;

    changed |= setUri(obj["uri"]);
    changed |= setStatus(obj["status"]);

    return changed;
}

bool UriData::
setUri(QJsonValue const& value)
{
    return updateValue(toString(value), m_uri);
}

bool UriData::
setStatus(QJsonValue const& value)
{
    auto const& status = toString(value);

    auto const old = m_status;

    if (status == "used") {
        m_status = URI_USED;
    }
    else if (status == "waiting") {
        m_status = URI_WAITING;
    }
    else {
        assert(false);
    }

    return m_status != old;
}

QDebug
operator<<(QDebug dbg, UriData const& uriData)
{
    dbg << uriData.m_uri;
    switch (uriData.m_status) {
    case UriData::URI_USED:
        dbg << " used";
        break;
    case UriData::URI_WAITING:
        dbg << " waiting";
        break;
    default:
        assert(false);
    }

    return dbg;
}

} // namespace aria2_remote
