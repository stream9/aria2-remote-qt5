#ifndef ARIA2_REMOTE_GLOBAL_OPTIONS_HPP
#define ARIA2_REMOTE_GLOBAL_OPTIONS_HPP

#include "options.hpp"

class QJsonObject;

namespace aria2_remote {

class Aria2Remote;

class GlobalOptions : public Options
{
public:
    GlobalOptions(Aria2Remote&);

private:
    void changeOption(QJsonObject const& options) override;
    void getOption(Callback const&) override;

private:
    Aria2Remote& m_aria2;
};


} // namespace aria2_remote

#endif // ARIA2_REMOTE_GLOBAL_OPTIONS_HPP
