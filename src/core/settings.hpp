#ifndef ARIA2_REMOTE_SETTINGS_HPP
#define ARIA2_REMOTE_SETTINGS_HPP

#include <QSettings>
#include <QString>

namespace aria2_remote {

class Settings : public QSettings
{
public:
    ~Settings() override;

    QStringList profiles() const;
    void setProfiles(QStringList const&);

    void beginProfile(QString const& name);
    void endProfile();

private:
    Settings();

    friend class Application;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_SETTINGS_HPP
