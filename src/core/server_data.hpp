#ifndef ARIA2_REMOTE_SERVER_DATA_HPP
#define ARIA2_REMOTE_SERVER_DATA_HPP

#include <cstdint>
#include <vector>

#include <QString>

class QJsonArray;
class QJsonObject;

namespace aria2_remote {

class ServerData
{
public:
    ServerData(QJsonObject const&);

    QString const& uri() const { return m_uri; }
    QString const& currentUri() const { return m_currentUri; }
    int32_t downloadSpeed() const { return m_downloadSpeed; }

    bool update(QJsonObject const&);

private:
    QString m_uri;
    QString m_currentUri;
    int32_t m_downloadSpeed;
};


class ServerDatas : public std::vector<ServerData>
{
public:
    ServerDatas() = default;
    ServerDatas(QJsonArray const&);

    bool update(QJsonArray const&);
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_SERVER_DATA_HPP
