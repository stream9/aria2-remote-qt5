#include "bt_meta_info_data.hpp"

#include "utility/json.hpp"

#include <cassert>

#include <boost/iterator/zip_iterator.hpp>
#include <boost/range/algorithm/equal.hpp>
#include <boost/range/iterator_range.hpp>
#include <boost/tuple/tuple.hpp>

#include <QJsonObject>
#include <QJsonArray>

namespace aria2_remote {

template<class T>
class dynamic_optional
{
public:
    dynamic_optional();

private:
    std::unique_ptr<T> m_value;
};

using util::toArray;
using util::toInt64;
using util::toObject;
using util::toString;

using AnnounceList = BtMetaInfoData::AnnounceList;

static AnnounceList
toAnnounceList(QJsonArray const& list)
{
    AnnounceList result;

    for (auto const& value: list) {
        auto const& trackers = toArray(value);
        result.emplace_back();
        auto& lastTier = result.back();

        for (auto const& value: trackers) {
            auto&& tracker = toString(value);
            lastTier.push_back(std::move(tracker));
        }
    }

    return result;
}

template<typename Range1, typename Range2>
static auto
zip(Range1 const& rng1, Range2 const& rng2)
{
    return boost::make_iterator_range(
        boost::make_zip_iterator(boost::make_tuple(rng1.begin(), rng2.begin())),
        boost::make_zip_iterator(boost::make_tuple(rng1.end(),   rng2.end()))
    );
}

static bool
operator==(AnnounceList const& left, AnnounceList const& right)
{
    using boost::range::equal;

    if (left.size() != right.size()) return false;

    for (auto const& tuple: zip(left, right)) {
        auto const& t1 = boost::get<0>(tuple);
        auto const& t2 = boost::get<1>(tuple);

        if (t1.size() != t2.size()) return false;

        if (!equal(t1, t2)) return false;
    }

    return true;
}

static bool
operator!=(AnnounceList const& left, AnnounceList const& right)
{
    return !(left == right);
}

// BtMetaInfoData

BtMetaInfoData::
BtMetaInfoData(QJsonObject const& obj)
{
    update(obj);
}

bool BtMetaInfoData::
update(QJsonObject const& obj)
{
    auto changed = false;

    if (auto const& value = obj.value("announceList"); !value.isUndefined()) {
        auto&& newList = toAnnounceList(toArray(value));

        if (newList != m_announceList) {
            m_announceList = std::move(newList);
            changed = true;
        }
    }
    else {
        assert(false && "There isn't announceList in bittorrent metainfo");
    }

    if (auto const& value = obj.value("comment"); !value.isUndefined()) {
        auto const& newComment = toString(value);

        if (!m_comment || *m_comment != newComment) {
            m_comment = newComment;
            changed = true;
        }
    }
    else {
        if (m_comment) {
            m_comment.reset();
            changed = true;
        }
    }

    if (auto const& value = obj.value("creationDate"); !value.isUndefined()) {
        auto const newDate = toInt64(value);
        if (newDate != m_creationDate) {
            m_creationDate = newDate;
            changed = true;
        }
    }
    else {
        if (m_creationDate) {
            m_creationDate.reset();
            changed = true;
        }
    }

    if (auto const& value = obj.value("mode"); !value.isUndefined()) {
        auto const& mode = toString(value);
        BtFileType newType;

        if (mode == "single") {
            newType = BtFileType::Single;
        }
        else if (mode == "multi") {
            newType = BtFileType::Multi;
        }
        else {
            qDebug() << "unknown bittorrent file mode:" << mode;
            throw std::logic_error("BtMetaInfoData::update");
        }

        if (newType != m_mode) {
            m_mode = newType;
            changed = true;
        }
    }
    else {
        if (m_mode) {
            m_mode.reset();
            changed = true;
        }
    }

    if (auto const& value = obj.value("info"); !value.isUndefined()) {
        auto const& info = toObject(value);

        if (auto const& value = info.value("name"); !value.isUndefined()) {
            auto const& newName = toString(value);

            if (!m_name || *m_name != newName) {
                m_name = newName;
                changed = true;
            }
        }
        else {
            assert(false && "No 'name' in 'info'");
        }
    }
    else {
        if (m_name) {
            m_name.reset();
            changed = true;
        }
    }

    return changed;
}

QDebug
operator<<(QDebug dbg, BtMetaInfoData const& data)
{
    QJsonObject obj;

    if (!data.announceList().empty()) {
        QJsonArray announceList;

        for (auto const& tier: data.announceList()) {
            if (tier.empty()) continue;

            QJsonArray arr;
            for (auto const& tracker: tier) {
                arr.push_back(tracker);
            }

            announceList.push_back(arr);
        }

        obj.insert("annouseList", announceList);
    }

    if (auto const& comment = data.comment()) {
        obj.insert("comment", *comment);
    }

    if (auto const& creationDate = data.creationDate()) {
        obj.insert("creationDate", static_cast<qint64>(*creationDate));
    }

    using Type = BtMetaInfoData::BtFileType;
    if (auto const& mode = data.mode(); mode) {
        if (*mode == Type::Single) {
            obj.insert("mode", "single");
        }
        else if (*mode == Type::Multi) {
            obj.insert("mode", "multi");
        }
    }

    if (auto const& name = data.name(); name) {
        obj.insert("name", *name);
    }

    dbg << obj;

    return dbg;
}

} // namespace aria2_remote
