#ifndef ARIA2_REMOTE_GLOBAL_STATUS_HPP
#define ARIA2_REMOTE_GLOBAL_STATUS_HPP

#include <cstdint>

#include <QObject>

class QDebug;
class QJsonObject;

namespace aria2_remote {

class Aria2Rpc;

class GlobalStatus : public QObject
{
    Q_OBJECT
public:
    GlobalStatus(Aria2Rpc&);

    int32_t downloadSpeed() const { return m_downloadSpeed; }
    int32_t uploadSpeed() const { return m_uploadSpeed; }
    int32_t numActive() const { return m_numActive; }
    int32_t numWaiting() const { return m_numWaiting; }
    int32_t numStopped() const { return m_numStopped; }
    int32_t numStoppedTotal() const { return m_numStoppedTotal; }

    void update();
    void update(QJsonObject const&);
    void clear();

    Q_SIGNAL void changed();

    friend QDebug operator<<(QDebug, GlobalStatus const&);

private:
    Aria2Rpc& m_aria2;

    int32_t m_downloadSpeed = 0;
    int32_t m_uploadSpeed = 0;
    int32_t m_numActive = 0;
    int32_t m_numWaiting = 0;
    int32_t m_numStopped = 0;
    int32_t m_numStoppedTotal = 0;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_GLOBAL_STATUS_HPP
