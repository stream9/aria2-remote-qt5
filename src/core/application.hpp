#ifndef ARIA2_REMOTE_APPLICATION_HPP
#define ARIA2_REMOTE_APPLICATION_HPP

#include "settings.hpp"

#include <memory>
#include <vector>

#include <QObject>

class QDir;
class QString;

namespace aria2_remote {

class ActionManager;
class Aria2Remote;
class DownloadHandle;
class ErrorHandler;
class MainWindow;
class Notification;
class Server;
class Servers;

class Application : public QObject
{
    Q_OBJECT
public:
    using Selection = std::vector<std::reference_wrapper<DownloadHandle>>;

public:
    Application();
    ~Application() override;

    QString applicationName() const;

    MainWindow& mainWindow() const;
    void setMainWindow(MainWindow& win) { m_mainWindow = &win; }

    ActionManager& actionManager() const { return *m_actionManager; }
    Aria2Remote* aria2() const { return m_aria2; }

    DownloadHandle* currentHandle() const { return m_currentHandle; }
    Selection const& selection() const { return m_selection; }

    Server* currentServer() const { return m_currentServer; }
    Servers& servers() const { return *m_servers; }
    void connectToServer(QString const& name);

    Settings&       settings() { return m_settings; }
    Settings const& settings() const { return m_settings; }

    Q_SLOT void setAria2(Aria2Remote&);
    Q_SLOT void clearAria2();

    Q_SLOT void setCurrentHandle(DownloadHandle&);
    Q_SLOT void clearCurrentHandle();

    Q_SLOT void setCurrentServer(Server&);
    Q_SLOT void clearCurrentServer();

    void setSelection(Selection&&);
    void clearSelection();

    Q_SIGNAL void aria2Changed(Aria2Remote*) const;
    Q_SIGNAL void currentHandleChanged(DownloadHandle*) const;
    Q_SIGNAL void selectionChanged() const;
    Q_SIGNAL void currentServerChanged(Server*) const;
    Q_SIGNAL void readSettings() const;
    Q_SIGNAL void writeSettings() const;

    static QDir configDir();

private:
    Q_SLOT void updateAria2();

private:
    Settings m_settings;
    std::unique_ptr<ErrorHandler> m_errorHandler; // non-null
    Aria2Remote* m_aria2 = nullptr;
    DownloadHandle* m_currentHandle = nullptr;
    Selection m_selection;
    std::unique_ptr<Servers> m_servers; // non-null
    Server* m_currentServer = nullptr;
    std::unique_ptr<ActionManager> m_actionManager; // non-null
    MainWindow* m_mainWindow;
    std::unique_ptr<Notification> m_notification; // non-null
};

Application& gApp();

} // namespace aria2_remote

#endif // ARIA2_REMOTE_APPLICATION_HPP
