#include "error_handler.hpp"

#include "application.hpp"
#include "server.hpp"

#include "gui/main_window.hpp"

#include <QString>
#include <QMessageBox>

namespace aria2_remote {

void ErrorHandler::
onConnectionError(Server const& server,
                  QString const& msg) const
{
    QString const fmt { "Connection error occur between server \"%1\": %2" };

    QMessageBox::critical(&gApp().mainWindow(),
                    gApp().applicationName(), fmt.arg(server.name()).arg(msg));
}

} // namespace aria2_remote
