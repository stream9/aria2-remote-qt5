#ifndef ARIA2_REMOTE_HANDLE_OPTIONS_HPP
#define ARIA2_REMOTE_HANDLE_OPTIONS_HPP

#include "options.hpp"

#include <memory>

namespace aria2_remote {

class DownloadHandle;

class HandleOptions : public Options
{
public:
    HandleOptions(DownloadHandle&);

private:
    void changeOption(QJsonObject const& options) override;
    void getOption(Callback const&) override;

private:
    std::shared_ptr<DownloadHandle> m_handle; // non-null
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_HANDLE_OPTIONS_HPP
