#include "profile.hpp"

#include "application.hpp"
#include "settings.hpp"

#include "json_rpc/response.hpp"

#include <cassert>

#include <QJsonObject>
#include <QJsonValue>

namespace aria2_remote {

Profile::
Profile(QString const& name)
    : m_name { name }
{
    this->refresh();
}

void Profile::
changeOption(QJsonObject const& options)
{
    auto& settings = gApp().settings();

    settings.beginProfile(m_name);

    for (auto it = options.begin(); it != options.end(); ++it) {
        settings.setValue(it.key(), it.value().toString());
    }

    settings.endProfile();
}

void Profile::
getOption(Callback const& callback)
{
    auto& settings = gApp().settings();
    QJsonObject options;

    settings.beginProfile(m_name);

    for (auto const& name: settings.allKeys()) {
        options.insert(name, settings.value(name).toString());
    }

    settings.endProfile();

    callback(json_rpc::Response::fromValue(options));
}

} // namespace aria2_remote
