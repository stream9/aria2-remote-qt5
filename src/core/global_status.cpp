#include "global_status.hpp"

#include "rpc/aria2_rpc.hpp"
#include "json_rpc/response.hpp"
#include "utility/json.hpp"

#include <cassert>

#include <QJsonObject>

#include <QDebug>

namespace aria2_remote {

using util::toInt32;
using util::toObject;

static bool
updateValue(int32_t& dest, QJsonValue const source)
{
    auto const src = toInt32(source);
    auto const changed = (src != dest);

    dest = src;

    return changed;
}

GlobalStatus::
GlobalStatus(Aria2Rpc& aria2)
    : m_aria2 { aria2 }
{}

void GlobalStatus::
update()
{
    if (m_aria2.isValid()) {
        m_aria2.getGlobalStat(
            [this](auto const& res) {
                if (res) {
                    this->update(toObject(res.value()));
                }
                else {
                    this->clear();
                }
            }
        );
    }
    else {
        clear();
    }
}

void GlobalStatus::
update(QJsonObject const& obj)
{
    auto changed = false;

    changed |= updateValue(m_downloadSpeed, obj.value("downloadSpeed"));
    changed |= updateValue(m_uploadSpeed, obj.value("uploadSpeed"));
    changed |= updateValue(m_numActive, obj.value("numActive"));
    changed |= updateValue(m_numWaiting, obj.value("numWaiting"));
    changed |= updateValue(m_numStopped, obj.value("numStopped"));
    changed |= updateValue(m_numStoppedTotal, obj.value("numStoppedTotal"));

    if (changed) {
        Q_EMIT this->changed();
    }
}

void GlobalStatus::
clear()
{
    m_downloadSpeed = 0;
    m_uploadSpeed = 0;
    m_numActive = 0;
    m_numWaiting = 0;
    m_numStopped = 0;
    m_numStoppedTotal = 0;

    Q_EMIT changed();
}

QDebug
operator<<(QDebug dbg, GlobalStatus const& status)
{
    dbg << "Download Speed:" << status.m_downloadSpeed << "\n"
        << "Upload Speed:" << status.m_uploadSpeed << "\n"
        << "# of Active:" << status.m_numActive << "\n"
        << "# of Waiting:" << status.m_numWaiting << "\n"
        << "# of Stopped:" << status.m_numStopped << "\n"
        << "# of Total Stopped:" << status.m_numStoppedTotal;

    return dbg;
}

} // namespace aria2_remote
