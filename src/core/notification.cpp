#include "notification.hpp"

#include "application.hpp"
#include "aria2_remote.hpp"
#include "download_handle.hpp"
#include "rpc/aria2_rpc.hpp"
#include "utility/qmanaged_ptr.hpp"

#include <QDebug>
#include <QProcess>
#include <QString>

namespace aria2_remote {

Notification::
Notification(Application& app)
    : m_application { app }
{
    auto& settings = app.settings();
    settings.beginGroup("Notification");

    auto const& value = settings.value("command");
    m_command = value.toString();

    settings.endGroup();

    this->connect(&m_application, &Application::aria2Changed,
                  this,           &Notification::onAria2Changed);
    onAria2Changed(m_application.aria2());
}

void Notification::
onAria2Changed(Aria2Remote* const aria2)
{
    if (!aria2) return;

    auto& rpc = aria2->rpc();

    this->connect(&rpc, &Aria2Rpc::onDownloadStart,
                  this, &Notification::onDownloadStart);
    this->connect(&rpc, &Aria2Rpc::onDownloadPause,
                  this, &Notification::onDownloadPause);
    this->connect(&rpc, &Aria2Rpc::onDownloadStop,
                  this, &Notification::onDownloadStop);
    this->connect(&rpc, &Aria2Rpc::onDownloadComplete,
                  this, &Notification::onDownloadComplete);
    this->connect(&rpc, &Aria2Rpc::onDownloadError,
                  this, &Notification::onDownloadError);
    this->connect(&rpc, &Aria2Rpc::onBtDownloadComplete,
                  this, &Notification::onBtDownloadComplete);
}

void Notification::
onDownloadStart(QString const& gid)
{
    launchCommand("onDownloadStart", gid);
}

void Notification::
onDownloadPause(QString const& gid)
{
    launchCommand("onDownloadPause", gid);
}

void Notification::
onDownloadStop(QString const& gid)
{
    launchCommand("onDownloadStop", gid);
}

void Notification::
onDownloadComplete(QString const& gid)
{
    launchCommand("onDownloadComplete", gid);
}

void Notification::
onDownloadError(QString const& gid)
{
    launchCommand("onDownloadError", gid);
}

void Notification::
onBtDownloadComplete(QString const& gid)
{
    launchCommand("onBtDownloadComplete", gid);
}

void Notification::
launchCommand(QString const& method, QString const& gid)
{
    if (m_command.isEmpty()) return;

    auto* const aria2 = m_application.aria2();
    if (!aria2) return;

    auto* const handle = aria2->downloadHandle(gid);
    if (!handle) return;

    auto const& uri = handle->uri();

    auto process = make_qmanaged<QProcess>(this);
    this->connect(process.get(), qOverload<int, QProcess::ExitStatus>(&QProcess::finished),
                  process.get(), &QObject::deleteLater);

    process->setProgram(m_command);

    QStringList args;
    args.push_back(method);
    args.push_back(uri);
    process->setArguments(args);

    process->startDetached();
}

} // namespace aria2_remote
