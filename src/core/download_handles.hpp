#ifndef ARIA2_REMOTE_DOWNLOAD_HANDLES_HPP
#define ARIA2_REMOTE_DOWNLOAD_HANDLES_HPP

#include <memory>
#include <vector>

#include <QObject>

class QJsonArray;
class QJsonValue;

namespace aria2_remote {

class Aria2Remote;
class DownloadHandle;

class DownloadHandles : public QObject
{
    Q_OBJECT

    using Handles = std::vector<std::shared_ptr<DownloadHandle>>;
    using const_iterator = Handles::const_iterator;

public:
    DownloadHandles(Aria2Remote&);

    const_iterator begin() const { return m_handles.begin(); }
    const_iterator end() const { return m_handles.end(); }

    DownloadHandle& operator[](size_t const index) const { return *m_handles[index]; }

    size_t size() const { return m_handles.size(); }

    bool update(QJsonValue const&);
    void clear();

    void refresh();

    Q_SIGNAL void changed() const;
    Q_SIGNAL void handleCreated(size_t index, DownloadHandle&) const;
    Q_SIGNAL void handleChanged(size_t index, DownloadHandle&) const;
    Q_SIGNAL void handleRemoved(size_t index) const;
    Q_SIGNAL void handleMoved(size_t from, size_t to, DownloadHandle&) const;

protected:
    Aria2Remote& aria2() { return m_aria2; }

private:
    void insertHandle(size_t const index, QJsonObject const& status);
    void removeHandle(size_t const index);
    void resolveDifference(QJsonArray const& statuses, size_t const index,
                    DownloadHandle const& handle, QJsonObject const& status);

    virtual void doRefresh() = 0;

private:
    Aria2Remote& m_aria2;
    Handles m_handles;
};

class ActiveHandles : public DownloadHandles
{
public:
    using DownloadHandles::DownloadHandles;

private:
    void doRefresh() override;
};

class WaitingHandles : public DownloadHandles
{
public:
    using DownloadHandles::DownloadHandles;

private:
    void doRefresh() override;
};

class StoppedHandles : public DownloadHandles
{
public:
    using DownloadHandles::DownloadHandles;

private:
    void doRefresh() override;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_DOWNLOAD_HANDLES_HPP
