#ifndef ARIA2_REMOTE_ERROR_HANDLER_HPP
#define ARIA2_REMOTE_ERROR_HANDLER_HPP

#include <QObject>

class QString;

namespace aria2_remote {

class Server;

class ErrorHandler : public QObject
{
public:
    Q_SLOT void onConnectionError(
                            Server const& server, QString const& msg) const;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_ERROR_HANDLER_HPP
