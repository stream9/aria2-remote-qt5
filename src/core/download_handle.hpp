#ifndef ARIA2_REMOTE_ITEM_HPP
#define ARIA2_REMOTE_ITEM_HPP

#include "bit_field.hpp"
#include "download_servers.hpp"
#include "download_status.hpp"
#include "download_uris.hpp"
#include "file_data.hpp"

#include "rpc/offset_mode.hpp"

#include <memory>
#include <optional>
#include <vector>

#include <boost/dynamic_bitset.hpp>

#include <QObject>
#include <QString>

class QJsonObject;
class QJsonValue;

namespace aria2_remote {

class Aria2Remote;
class BtMetaInfoData;

class DownloadHandle : public QObject
                     , public std::enable_shared_from_this<DownloadHandle>
{
    Q_OBJECT
public:
    using FileDatas = std::vector<FileData>;
    using UriDatas = DownloadUris::UriDatas;

public:
    DownloadHandle(Aria2Remote&, QString const& gid, QJsonObject const&);
    ~DownloadHandle();

    // accessor
    Aria2Remote& aria2() { return m_aria2; }

    // query
    QString const& gid() const { return m_gid; }
    DownloadStatus const& status() const { return m_status; }
    int64_t totalLength() const { return m_totalLength; }
    int64_t completedLength() const { return m_completedLength; }
    int64_t uploadLength() const { return m_uploadLength; }
    BitField const& bitField() const { return m_bitField; }
    int32_t downloadSpeed() const { return m_downloadSpeed; }
    int32_t uploadSpeed() const { return m_uploadSpeed; }
    QString const* infoHash() const;
    std::optional<int64_t> const& numSeeders() const { return m_numSeeders; }
    std::optional<bool> const& seeder() const { return m_seeder; }
    int64_t pieceLength() const { return m_pieceLength; }
    int32_t numPieces() const { return m_numPieces; }
    int32_t connections() const { return m_connections; }
    QString errorMessage() const { return m_errorMessage; }
    std::vector<QString> const* followedBy() const;
    QString const* following() const;
    QString const* belongsTo() const;
    QString const& dir() const { return m_dir; }
    FileDatas const& files() const { return m_files; }
    BtMetaInfoData const* bitTorrent() const;
    std::optional<int64_t> const& verifiedLength() const { return m_verifiedLength; }
    std::optional<bool> const& veryfyIntegrityPending() const { return m_verifyIntegrityPending; }

    QString uri() const;
    UriDatas const& uris() const { return m_uris->uris(); }
    ServerDatas const& servers(int32_t fileIndex) const;

    bool isTorrent() const { return infoHash(); }

    // modifier
    void pause();
    void unpause();
    void remove();
    void changePosition(int32_t pos, OffsetMode);

    bool update(QJsonObject const&);

    void setUpdateUris(bool);
    void setUpdateServers(bool);

    Q_SIGNAL void changed() const;
    Q_SIGNAL void statusChanged() const;
    Q_SIGNAL void urisChanged() const;
    Q_SIGNAL void serversChanged() const;

private:
    bool setFiles(QJsonValue const&);
    bool setBitField(QJsonValue const&);
    bool setFollowedBy(QJsonValue const&);
    bool setBitTorrent(QJsonValue const&);

    friend QDebug operator<<(QDebug, DownloadHandle const&);

private:
    Aria2Remote& m_aria2;
    QString m_gid;
    DownloadStatus m_status;
    int64_t m_totalLength;
    int64_t m_completedLength;
    int64_t m_uploadLength;
    BitField m_bitField;
    int32_t m_downloadSpeed;
    int32_t m_uploadSpeed;
    QString m_infoHash; // optional, bt only
    std::optional<int64_t> m_numSeeders; // bt only
    std::optional<bool> m_seeder; // bt only
    int64_t m_pieceLength;
    int32_t m_numPieces;
    int32_t m_connections;
    int32_t m_errorCode;
    QString m_errorMessage;
    std::vector<QString> m_followedBy; // optional
    QString m_following; // optional
    QString m_belongsTo; // optional
    QString m_dir;
    FileDatas m_files;
    std::unique_ptr<BtMetaInfoData> m_bitTorrent; // bt only
    std::optional<int64_t> m_verifiedLength;
    std::optional<bool> m_verifyIntegrityPending;

    std::unique_ptr<DownloadUris> m_uris;
    bool m_updateUris = false;

    DownloadServers m_servers;
    bool m_updateServers = false;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_ITEM_HPP
