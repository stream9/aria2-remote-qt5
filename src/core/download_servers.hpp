#ifndef ARIA2_REMOTE_DOWNLOAD_SERVERS_HPP
#define ARIA2_REMOTE_DOWNLOAD_SERVERS_HPP

#include "server_data.hpp"

#include <cstdint>
#include <vector>

#include <boost/container/flat_map.hpp>

#include <QObject>
#include <QString>

class QJsonArray;
class QJsonObject;
class QJsonValue;

namespace aria2_remote {

class Aria2Rpc;

class DownloadServers : public QObject
{
    Q_OBJECT
public:
    using FileIndex = int32_t;
    using ServerMap = boost::container::flat_map<FileIndex, ServerDatas>;

public:
    DownloadServers(Aria2Rpc&, QString const& gid);

    ServerDatas const& servers(FileIndex) const;

    void update();

    Q_SIGNAL void changed() const;

private:
    void setServers(QJsonValue const&);
    bool updateServers(FileIndex, QJsonArray const&);
    void clear();

private:
    Aria2Rpc& m_aria2;

    QString const m_gid;

    ServerMap m_servers;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_DOWNLOAD_SERVERS_HPP
