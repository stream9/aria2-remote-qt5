#include "download_peers.hpp"

#include "aria2_remote.hpp"
#include "download_handle.hpp"
#include "json_rpc/response.hpp"
#include "utility/json.hpp"
#include "rpc/aria2_rpc.hpp"

#include <chrono>

#include <QJsonArray>
#include <QJsonObject>

#include <QDebug>

#include <stream9/myers_diff.hpp>

namespace aria2_remote {

using util::toArray;
using util::toBool;
using util::toUInt16;
using util::toInt32;
using util::toObject;
using util::toString;

namespace myers_diff = stream9::myers_diff;

// Peers

Peer::
Peer(QJsonObject const& obj, DownloadHandle const& handle)
    : m_id { toString(obj["peerId"]) }
    , m_address { toString(obj["ip"]) }
    , m_port { toUInt16(obj["port"]) }
    , m_bitField { toString(obj["bitfield"]), handle.numPieces() }
    , m_downloadSpeed { toInt32(obj["downloadSpeed"]) }
    , m_uploadSpeed { toInt32(obj["uploadSpeed"]) }
    , m_choking { toBool(obj["amChoking"]) }
    , m_choked { toBool(obj["peerChoking"]) }
    , m_seeder { toBool(obj["seeder"]) }
{}

bool Peer::
operator==(Peer const& other) const
{
    return m_id == other.m_id
        && m_address == other.m_address
        && m_port == other.m_port
        && m_bitField == other.m_bitField
        && m_downloadSpeed == other.m_downloadSpeed
        && m_uploadSpeed == other.m_uploadSpeed
        && m_choking == other.m_choking
        && m_choked == other.m_choked
        && m_seeder == other.m_seeder
        ;
}

// DownloadPeers

DownloadPeers::
DownloadPeers(DownloadHandle& handle)
    : m_handle { &handle }
{
    assert(m_handle);

    using namespace std::literals;
    m_timer.start(2s);

    this->connect(m_handle, &DownloadHandle::destroyed,
                  this,     &DownloadPeers::onHandleDestroyed);
    this->connect(&m_timer, &QTimer::timeout,
                  this,     &DownloadPeers::update);
}

void DownloadPeers::
update()
{
    if (!m_handle) return;

    m_handle->aria2().rpc().getPeers(m_handle->gid(),
        [this, self = this->shared_from_this()]
        (auto const& res) {
            auto&& peers = res ? createPeers(toArray(res.value()))
                               : Peers {};
            this->updatePeers(peers);
        }
    );
}

void DownloadPeers::
onHandleDestroyed()
{
    clear();

    m_handle = nullptr;

    m_timer.stop();
}

DownloadPeers::Peers DownloadPeers::
createPeers(QJsonArray const& arr) const
{
    Peers result;

    if (!m_handle) return result;

    for (auto const& element: arr) {
        auto const& peer = toObject(element);

        result.emplace_back(peer, *m_handle);
    }

    return result;
}

template<typename Peers>
struct Visitor : myers_diff::event_handler
{
    using index_t = myers_diff::uindex_t;

    Visitor(DownloadPeers& parent,
            Peers const& before, Peers const& after)
        : m_parent { parent }
        , m_before { before }
        , m_after { after }
    {}

    void unchanged(uindex_t index1, uindex_t index2) override
    {
        if (m_before[index1] != m_after[index2]) {
            m_parent.peerChanged(m_current, m_after[index2]);
        }

        ++m_current;
    }

    void inserted(uindex_t index) override
    {
        m_parent.peerCreated(m_current, m_after[index]);

        ++m_current;
    }

    void deleted(uindex_t index) override
    {
        m_parent.peerDeleted(m_current, m_before[index]);
    }

    DownloadPeers& m_parent;
    Peers const& m_before;
    Peers const& m_after;
    mutable size_t m_current = 0;
};

void DownloadPeers::
updatePeers(Peers& peers)
{
    m_peers.swap(peers);

    Visitor visitor { *this, peers, m_peers };

    auto identical = [&](auto const index1, auto const index2) {
       return peers[index1].address() == m_peers[index2].address()
           && peers[index1].port() == m_peers[index2].port();
    };

    myers_diff::diff(peers.size(), m_peers.size(), identical, visitor);
}

void DownloadPeers::
clear()
{
    m_peers.clear();

    Q_EMIT cleared();
}

} // namespace aria2_remote
