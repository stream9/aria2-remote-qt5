#ifndef ARIA2_REMOTE_BT_META_INFO_DATA_HPP
#define ARIA2_REMOTE_BT_META_INFO_DATA_HPP

#include <cstdint>
#include <memory>
#include <optional>
#include <vector>

#include <QString>
#include <QDebug>

class QJsonArray;
class QJsonObject;

namespace aria2_remote {

class BtMetaInfoData
{
public:
    enum class BtFileType { Single, Multi };
    using AnnounceList = std::vector<std::vector<QString>>;

public:
    BtMetaInfoData(QJsonObject const&);

    AnnounceList const& announceList() const { return m_announceList; }
    std::optional<QString> const& comment() const { return m_comment; }
    std::optional<int64_t> const& creationDate() const { return m_creationDate; }
    std::optional<BtFileType> const& mode() const { return m_mode; }
    std::optional<QString> const& name() const { return m_name; }

    bool update(QJsonObject const&);

private:
    AnnounceList m_announceList;
    std::optional<QString> m_comment;
    std::optional<int64_t> m_creationDate;
    std::optional<BtFileType> m_mode;
    std::optional<QString> m_name;
};

QDebug operator<<(QDebug, BtMetaInfoData const&);

} // namespace aria2_remote

#endif // ARIA2_REMOTE_BT_META_INFO_DATA_HPP
