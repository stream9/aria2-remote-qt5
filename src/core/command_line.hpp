#ifndef ARIA2_REMOTE_QT5_CORE_COMMAND_LINE_HPP
#define ARIA2_REMOTE_QT5_CORE_COMMAND_LINE_HPP

#include <QString>

namespace aria2_remote {

class CommandLine
{
public:
    CommandLine(int argc, char *argv[]);

    // query
    QString const& server() const { return m_server; }

    // command
    void printUsage() const;

private:
    int m_argc;
    char** m_argv;
    QString m_server;
};

void parseCommandLine(int argc, char* argv[]);

CommandLine const& gCommandLine();

} // namespace aria2_remote

#endif // ARIA2_REMOTE_QT5_CORE_COMMAND_LINE_HPP
