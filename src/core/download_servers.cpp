#include "download_servers.hpp"

#include "json_rpc/response.hpp"
#include "utility/json.hpp"
#include "rpc/aria2_rpc.hpp"

#include <boost/numeric/conversion/cast.hpp>

#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>

namespace aria2_remote {

using util::toArray;
using util::toInt32;
using util::toObject;

DownloadServers::
DownloadServers(Aria2Rpc& aria2, QString const& gid)
    : m_aria2 { aria2 }
    , m_gid { gid }
{}

ServerDatas const& DownloadServers::
servers(FileIndex const index) const
{
    static ServerDatas const empty;

    auto const it = m_servers.find(index);
    if (it == m_servers.end()) {
        return empty;
    }
    else {
        return it->second;
    }

}

void DownloadServers::
update()
{
    m_aria2.getServers(m_gid,
        [this](auto const& res) {
            if (res) {
                this->setServers(res.value());
            }
            else {
                this->clear();
            }
        }
    );
}

void DownloadServers::
setServers(QJsonValue const& json)
{
    bool changed = false;

    auto const& files = toArray(json);

    for (auto const& element: files) {
        auto const& file = toObject(element);

        auto const index = toInt32(file.value("index"));
        auto const& servers = toArray(file.value("servers"));

        changed |= updateServers(index, servers);
    }

    if (changed) {
        Q_EMIT this->changed();
    }
}

bool DownloadServers::
updateServers(FileIndex const index, QJsonArray const& array)
{
    bool changed = false;

    auto it = m_servers.find(index);

    if (it == m_servers.end()) {
        m_servers.emplace(index, ServerDatas { array });
        changed = true;
    }
    else if (boost::numeric_cast<size_t>(array.size()) == it->second.size()) {
        auto& servers = it->second;

        servers.update(array);
    }
    else {
        it->second = ServerDatas { array };
        changed = true;
    }

    return changed;
}

void DownloadServers::
clear()
{
    m_servers.clear();
    Q_EMIT changed();
}

} // namespace aria2_remote
