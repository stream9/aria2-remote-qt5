#include "download_status.hpp"

#include "utility/json.hpp"

#include <cassert>

#include <QJsonValue>

namespace aria2_remote {

using util::toString;

static QString const s_active { "active" };
static QString const s_waiting { "waiting" };
static QString const s_paused { "paused" };
static QString const s_error { "error" };
static QString const s_complete { "complete" };
static QString const s_removed { "removed" };
static QString const s_invalid { "invalid" };

// DownloadStatus

DownloadStatus::
DownloadStatus()
    : m_value { Invalid }
{}

bool DownloadStatus::
isValid() const
{
    return m_value != Invalid;
}

QString const& DownloadStatus::
text() const
{
    switch (m_value) {
    case Active:
        return s_active;
    case Waiting:
        return s_waiting;
    case Paused:
        return s_paused;
    case Error:
        return s_error;
    case Complete:
        return s_complete;
    case Removed:
        return s_removed;
    case Invalid:
    default:
        return s_invalid;
    }
}

bool DownloadStatus::
update(QJsonValue const& value)
{
    auto const& status = toString(value);
    auto const old = m_value;

    if (status == s_active) {
        m_value = Active;
    }
    else if (status == s_waiting) {
        m_value = Waiting;
    }
    else if (status == s_paused) {
        m_value = Paused;
    }
    else if (status == s_error) {
        m_value = Error;
    }
    else if (status == s_complete) {
        m_value = Complete;
    }
    else if (status == s_removed) {
        m_value = Removed;
    }
    else if (status == s_invalid) {
        m_value = Invalid;
    }

    return m_value != old;
}

} // namespace aria2_remote
