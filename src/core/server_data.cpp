#include "server_data.hpp"

#include "utility/json.hpp"

#include <cassert>

#include <boost/numeric/conversion/cast.hpp>

#include <QJsonArray>
#include <QJsonObject>

namespace aria2_remote {

using util::toInt32;
using util::toObject;
using util::toString;

template<typename T>
static bool
updateValue(T& dest, T const& src)
{
    bool const changed = (dest != src);

    if (changed) {
        dest = src;
    }

    return changed;
}

// ServerData

ServerData::
ServerData(QJsonObject const& obj)
    : m_uri { toString(obj.value("uri")) }
    , m_currentUri { toString(obj.value("currentUri")) }
    , m_downloadSpeed { toInt32(obj.value("downloadSpeed")) }
{}

bool ServerData::
update(QJsonObject const& obj)
{
    bool changed = false;

    changed |= updateValue(m_uri, toString(obj.value("uri")));
    changed |= updateValue(m_currentUri, toString(obj.value("currentUri")));
    changed |= updateValue(m_downloadSpeed, toInt32(obj.value("downloadSpeed")));

    return changed;
}

// ServerDatas

ServerDatas::
ServerDatas(QJsonArray const& array)
{
    this->reserve(static_cast<size_t>(array.size()));

    for (auto const& element: array) {
        auto const& server = toObject(element);

        this->emplace_back(server);
    }
}

bool ServerDatas::
update(QJsonArray const& array)
{
    assert(boost::numeric_cast<size_t>(array.size()) == this->size());

    bool changed = false;

    for (qsizetype idx = 0, len = array.size(); idx < len; ++idx) {
        auto& server = (*this)[static_cast<size_t>(idx)];

        changed |= server.update(toObject(array[idx]));
    }

    return changed;
}


} // namespace aria2_remote
