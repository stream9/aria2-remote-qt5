#ifndef ARIA2_REMOTE_OPTIONS_HPP
#define ARIA2_REMOTE_OPTIONS_HPP

#include "json_rpc/types.hpp"

#include <boost/container/flat_map.hpp>

#include <QObject>
#include <QString>

class QJsonObject;
class QDebug;

namespace aria2_remote {

class Options : public QObject
{
    Q_OBJECT

    using Container = boost::container::flat_map<QString, QString>;

protected:
    using Callback = json_rpc::Callback;

public:
    Options() = default;

    auto begin() const { return m_options.begin(); }
    auto end() const { return m_options.end(); }

    QString const* value(QString const& name) const;

    void setValue(QString const& name, QString const& value);
    void setValues(QJsonObject const&);

    bool update(QJsonObject const&);

    QJsonObject toJson() const;

    Q_SIGNAL void changed() const;
    Q_SIGNAL void updated() const;

protected:
    Container& options() { return m_options; }

    void refresh();

private:
    virtual void changeOption(QJsonObject const& options) = 0;
    virtual void getOption(Callback const&) = 0;

private:
    Container m_options;

    friend QDebug operator<<(QDebug, Options const&);
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_OPTIONS_HPP
