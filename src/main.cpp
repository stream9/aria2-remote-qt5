#include <iostream>

#include <QApplication>

#include "core/application.hpp"
#include "core/command_line.hpp"
#include "gui/main_window.hpp"

#include <boost/preprocessor/stringize.hpp>

int main(int argc, char *argv[])
{
    QApplication app { argc, argv };

    QCoreApplication::setOrganizationName("stream9");
    QCoreApplication::setApplicationName("aria2-remote-qt");
    QCoreApplication::setApplicationVersion(BOOST_PP_STRINGIZE(VERSION));

    aria2_remote::parseCommandLine(argc, argv);

    aria2_remote::gApp();
    aria2_remote::MainWindow mainWin;

    mainWin.show();

    return app.exec();
}
