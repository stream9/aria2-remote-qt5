#include "status_bar.hpp"

#include "core/application.hpp"
#include "core/aria2_remote.hpp"
#include "core/global_status.hpp"

#include "utility/format.hpp"

#include <cassert>

#include <QLabel>

#include <QDebug>

namespace aria2_remote {

using util::formatBandwidth;

static QString
activeText(int32_t const num)
{
    static QString fmt { "Active: %1" };
    return fmt.arg(num);
}

static QString
waitingText(int32_t const num)
{
    static QString fmt { "Waiting: %1" };
    return fmt.arg(num);
}

static QString
stoppedText(int32_t const num, int32_t const total)
{
    static QString fmt { "Stopped: %1 (%2)" };
    return fmt.arg(num).arg(total);
}

static QString
speedString(int32_t const up, int32_t const down)
{
    assert(up >= 0);
    assert(down >= 0);

    static QString fmt { "Up / Down: %1 / %2" };

    return fmt.arg(formatBandwidth(static_cast<uint64_t>(up)))
              .arg(formatBandwidth(static_cast<uint64_t>(down)));
}

// StatusBar

StatusBar::
StatusBar()
    : m_active { make_qmanaged<QLabel>(this) }
    , m_waiting { make_qmanaged<QLabel>(this) }
    , m_stopped { make_qmanaged<QLabel>(this) }
    , m_speed { make_qmanaged<QLabel>(this) }
{
    this->setSizeGripEnabled(false);

    auto spacer = make_qmanaged<QWidget>();
    this->addWidget(spacer.get(), 1);

    m_active->setText(activeText(0));
    this->addWidget(m_active.get());

    m_waiting->setText(waitingText(0));
    this->addWidget(m_waiting.get());

    m_stopped->setText(stoppedText(0, 0));
    this->addWidget(m_stopped.get());

    m_speed->setText(speedString(0, 0));
    this->addWidget(m_speed.get());

    this->connect(&gApp(), &Application::aria2Changed,
                  this,    &StatusBar::updateAria2);

    updateAria2();
}

void StatusBar::
updateAria2()
{
    if (m_aria2) {
        m_aria2->disconnect(this);
    }

    m_aria2 = gApp().aria2();

    if (m_aria2) {
        this->connect(m_aria2, &Aria2Remote::globalStatusChanged,
                      this,    &StatusBar::refresh);
    }

    refresh();
}

void StatusBar::
refresh()
{
    m_active->setEnabled(!!m_aria2);
    m_waiting->setEnabled(!!m_aria2);
    m_stopped->setEnabled(!!m_aria2);
    m_speed->setEnabled(!!m_aria2);

    if (m_aria2) {
        auto const& status = m_aria2->globalStatus();

        m_active->setText(activeText(status.numActive()));

        m_waiting->setText(waitingText(status.numWaiting()));

        m_stopped->setText(
            stoppedText(status.numStopped(), status.numStoppedTotal())
        );

        m_speed->setText(
            speedString(status.uploadSpeed(), status.downloadSpeed())
        );
    }
    else {
        m_active->setText(activeText(0));
        m_waiting->setText(waitingText(0));
        m_stopped->setText(stoppedText(0, 0));
        m_speed->setText(speedString(0, 0));
    }
}

} // namespace aria2_remote
