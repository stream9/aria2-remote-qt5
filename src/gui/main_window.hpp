#ifndef MAIN_WINDOW_HPP
#define MAIN_WINDOW_HPP

#include "utility/pointer.hpp"

#include <QMainWindow>

class QCloseEvent;

namespace aria2_remote {

class ActionManager;
class Aria2Remote;
class DownloadsView;
class FilesPanel;
class GeneralPanel;
class PeersPanel;
class ServersPanel;
class StatusBar;
class TorrentPanel;
class UrisPanel;

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    MainWindow();
    ~MainWindow() override;

    // override QMainWindow
    QSize sizeHint() const override;

protected:
    // override QMainWindow
    void closeEvent(QCloseEvent*) override;

private:
    void readSettings();
    void writeSettings();

    void createMenuBar();
    void createFileMenu();
    void createViewMenu();

    void createToolBar();

    Q_SLOT void onAria2Changed(Aria2Remote*);

private:
    qmanaged_ptr<DownloadsView> m_downloadsView; // non-null
    qmanaged_ptr<GeneralPanel> m_generalPanel; // non-null
    qmanaged_ptr<FilesPanel> m_filesPanel; // non-null
    qmanaged_ptr<UrisPanel> m_urisPanel; // non-null
    qmanaged_ptr<ServersPanel> m_serversPanel; // non-null
    qmanaged_ptr<PeersPanel> m_peersPanel; // non-null
    qmanaged_ptr<StatusBar> m_statusBar; // non-null
};

} // namespace aria2_remote

#endif // MAIN_WINDOW_HPP
