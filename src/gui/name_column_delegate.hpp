#ifndef ARIA2_REMOTE_QT5_GUI_NAME_COLUMN_DELEGATE_HPP
#define ARIA2_REMOTE_QT5_GUI_NAME_COLUMN_DELEGATE_HPP

#include <QStyledItemDelegate>

class QModelIndex;
class QObject;
class QPainter;
class QStyleOptionViewItem;

namespace aria2_remote {

class NameColumnDelegate : public QStyledItemDelegate
{
public:
    NameColumnDelegate(QObject* const parent = nullptr);

    void paint(QPainter*, QStyleOptionViewItem const&, QModelIndex const&) const override;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_QT5_GUI_NAME_COLUMN_DELEGATE_HPP
