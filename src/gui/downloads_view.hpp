#ifndef ARIA2_REMOTE_DOWNLOADS_VIEW
#define ARIA2_REMOTE_DOWNLOADS_VIEW

#include "tree_view.hpp"

#include <memory>

class QItemSelection;
class QModelIndex;
class QPoint;

namespace aria2_remote {

class Application;
class Aria2Remote;
class DownloadHandle;
class DownloadsModel;

class DownloadsView : public TreeView
{
    Q_OBJECT
public:
    DownloadsView();
    ~DownloadsView() override;

protected:
    // override QWidget
    void contextMenuEvent(QContextMenuEvent* const) override;

private:
    void createModel();

    Q_SLOT void onSelectionChanged(QItemSelection const& selected,
                                   QItemSelection const& deselected);

    Q_SLOT void onActivated(QModelIndex const&);

private:
    std::unique_ptr<DownloadsModel> m_model; // non-null
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_DOWNLOADS_VIEW
