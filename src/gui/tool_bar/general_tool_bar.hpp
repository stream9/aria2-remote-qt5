#ifndef ARIA2_REMOTE_TOOL_BAR_GENERAL_TOOL_BAR_HPP
#define ARIA2_REMOTE_TOOL_BAR_GENERAL_TOOL_BAR_HPP

#include <QToolBar>

namespace aria2_remote {

class ActionManager;

class GeneralToolBar : public QToolBar
{
public:
    GeneralToolBar(ActionManager&);
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_TOOL_BAR_GENERAL_TOOL_BAR_HPP
