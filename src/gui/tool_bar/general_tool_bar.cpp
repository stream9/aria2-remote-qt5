#include "general_tool_bar.hpp"

#include "gui/action_manager.hpp"
#include "utility/pointer.hpp"

#include <QSize>
#include <QMenu>
#include <QToolButton>

namespace aria2_remote {

static QToolButton&
createAddButton(QWidget& parent, ActionManager& actionManager)
{
    auto menu = make_qmanaged<QMenu>(&parent);

    menu->addAction(&actionManager.addUriAction());
    menu->addAction(&actionManager.addTorrentAction());
    menu->addAction(&actionManager.addMetalinkAction());

    auto button = make_qmanaged<QToolButton>(&parent);

    button->setMenu(menu.get());
    button->setPopupMode(QToolButton::MenuButtonPopup);
    button->setDefaultAction(&actionManager.addUriAction());

    return *button;
}

GeneralToolBar::
GeneralToolBar(ActionManager& actionManager)
    : QToolBar { "General Toolbar" }
{
    this->setObjectName("GeneralToolBar");
    this->setIconSize(QSize(16, 16));

    this->addWidget(&createAddButton(*this, actionManager));

    this->addAction(&actionManager.pauseAction());
    this->addAction(&actionManager.removeAction());
    this->addAction(&actionManager.removeCompletedAction());
    this->addAction(&actionManager.moveUpAction());
    this->addAction(&actionManager.moveDownAction());
    this->addAction(&actionManager.optionAction());
    this->addAction(&actionManager.propertyAction());
}

} // namespace aria2_remote
