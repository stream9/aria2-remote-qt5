#include "server_tool_bar.hpp"

#include "../action_manager.hpp"

namespace aria2_remote {

ServerToolBar::
ServerToolBar(ActionManager& actionManager)
    : QToolBar { "Server Toolbar" }
{
    this->setObjectName("ServerToolBar");
    this->setIconSize(QSize(16, 16));

    this->addAction(&actionManager.connectAction());
}

} // namespace aria2_remote
