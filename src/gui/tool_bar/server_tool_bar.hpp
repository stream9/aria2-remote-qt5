#ifndef ARIA2_REMOTE_TOOL_BAR_SERVER_TOOL_BAR_HPP
#define ARIA2_REMOTE_TOOL_BAR_SERVER_TOOL_BAR_HPP

#include <QToolBar>

namespace aria2_remote {

class ActionManager;

class ServerToolBar : public QToolBar
{
public:
    ServerToolBar(ActionManager&);
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_TOOL_BAR_SERVER_TOOL_BAR_HPP
