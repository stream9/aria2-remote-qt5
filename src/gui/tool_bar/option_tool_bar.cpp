#include "option_tool_bar.hpp"

#include "core/application.hpp"
#include "core/aria2_remote.hpp"
#include "core/options.hpp"
#include "core/server.hpp"

#include <cassert>

#include <QSpinBox>

#include <QDebug>

namespace aria2_remote {

// OptionWidget

class OptionWidget : public QSpinBox
{
    Q_OBJECT
public:
    OptionWidget(OptionToolBar& toolBar)
        : QSpinBox { &toolBar }
        , m_toolBar { toolBar }
    {
        this->setAlignment(Qt::AlignRight);

        this->connect(
            this, qOverload<int>(&QSpinBox::valueChanged),
            this, &OptionWidget::onValueChanged);

        update();
    }

    Q_SLOT void update()
    {
        if (!m_toolBar.aria2()) {
            this->setEnabled(false);
            this->setValue(0);
        }
        else {
            auto const& value = m_toolBar.optionValue(optionName());

            if (value) {
                this->setEnabled(true);
                this->setValue(value.value());
            }
            else {
                this->setEnabled(false);
                this->setValue(0);
            }
        }
    }

private:
    virtual QString optionName() const = 0;

    virtual QString valueText() const
    {
        return this->cleanText();
    }

    Q_SLOT void onValueChanged(int const value)
    {
        auto const& currentValue = m_toolBar.optionValue(optionName());
        if (!currentValue) {
            this->setEnabled(false);
            return;
        }

        if (value != currentValue.value()) {
            m_toolBar.setOptionValue(optionName(), value);
        }
    }

private:
    OptionToolBar& m_toolBar;
};

// MaxConcurrentDownloadWidget

class MaxConcurrentDownloadWidget : public OptionWidget
{
public:
    MaxConcurrentDownloadWidget(OptionToolBar& toolBar)
        : OptionWidget { toolBar }
    {
        this->setToolTip("Max Concurrent Downloads");
        this->setMinimum(1);
    }

private:
    QString optionName() const override
    {
        return QStringLiteral("max-concurrent-downloads");
    }
};

// MaxDownloadSpeed

class MaxDownloadSpeed : public OptionWidget
{
public:
    MaxDownloadSpeed(OptionToolBar& toolBar)
        : OptionWidget { toolBar }
    {
        this->setToolTip("Max Overall Download Speed");
        this->setMaximum(9999);
        this->setSuffix("KB/s");
    }

private:
    QString optionName() const override
    {
        return QStringLiteral("max-overall-download-limit");
    }

    QString valueText() const override
    {
        return QString::number(this->value() * 1024);
    }
};

// MaxUploadSpeed

class MaxUploadSpeed : public OptionWidget
{
public:
    MaxUploadSpeed(OptionToolBar& toolBar)
        : OptionWidget { toolBar }
    {
        this->setToolTip("Max Overall Upload Speed");
        this->setMaximum(9999);
        this->setSuffix("KB/s");
    }

private:
    QString optionName() const override
    {
        return QStringLiteral("max-overall-upload-limit");
    }

    QString valueText() const override
    {
        return QString::number(this->value() * 1024);
    }
};

// Updater

class Updater : public QObject
{
    Q_OBJECT
public:
    Updater(OptionToolBar& toolBar)
        : m_toolBar { toolBar }
    {

        m_timer.setSingleShot(true);
        m_timer.setInterval(500);

        this->connect(&m_timer, &QTimer::timeout,
                      this,     &Updater::doUpdate);
    }

    void update(QString const& name, int const value)
    {
        m_name = name;
        m_value = value;
        m_timer.start();
    }

private:
    Q_SLOT void doUpdate()
    {
        auto* const aria2 = m_toolBar.aria2();
        if (!aria2) return;

        assert(!m_name.isEmpty());
        auto& options = aria2->globalOptions();
        options.setValue(m_name, QString::number(m_value, 10));
    }

private:
    OptionToolBar& m_toolBar;
    QTimer m_timer;
    QString m_name;
    int m_value;
};

// OptionToolBar

OptionToolBar::
OptionToolBar(Application& app)
    : QToolBar { "Option Toolbar" }
    , m_application { app }
    , m_maxConcurrentDownload { make_qmanaged<MaxConcurrentDownloadWidget>(*this) }
    , m_maxDownloadSpeed { make_qmanaged<MaxDownloadSpeed>(*this) }
    , m_maxUploadSpeed { make_qmanaged<MaxUploadSpeed>(*this) }
    , m_updater { std::make_unique<Updater>(*this) }
{
    this->setObjectName("OptionToolBar");
    this->setIconSize(QSize(16, 16));

    this->addWidget(m_maxConcurrentDownload.get());
    this->addWidget(m_maxDownloadSpeed.get());
    this->addWidget(m_maxUploadSpeed.get());

    this->connect(&m_application, &Application::aria2Changed,
                  this,           &OptionToolBar::updateAria2);

    updateAria2();
}

OptionToolBar::~OptionToolBar() = default;

std::optional<int> OptionToolBar::
optionValue(QString const& name) const
{
    if (!m_aria2) return std::nullopt;

    auto& options = m_aria2->globalOptions();
    auto const& option = options.value(name);

    if (option) {
        return option->toInt();
    }
    else {
        return std::nullopt;
    }
}

void OptionToolBar::
setOptionValue(QString const& name, int const value) const
{
    assert(m_aria2);

    m_updater->update(name, value);
}

void OptionToolBar::
updateWidgets()
{
    m_maxConcurrentDownload->update();
    m_maxDownloadSpeed->update();
    m_maxUploadSpeed->update();
}

void OptionToolBar::
updateAria2()
{
    if (m_aria2) {
        m_aria2->disconnect(this);
    }

    m_aria2 = m_application.aria2();

    if (m_aria2) {
        this->connect(&m_aria2->globalOptions(), &Options::changed,
                      this,                      &OptionToolBar::updateWidgets);
    }

    updateWidgets();
}

#include "option_tool_bar.moc"

} // namespace aria2_remote
