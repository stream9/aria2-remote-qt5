#ifndef ARIA2_REMOTE_TREE_VIEW_HPP
#define ARIA2_REMOTE_TREE_VIEW_HPP

#include <QTreeView>

class QPoint;

namespace aria2_remote {

class TreeView : public QTreeView
{
    Q_OBJECT
public:
    TreeView(QWidget* parent = nullptr);

    virtual void readSettings();
    virtual void writeSettings() const;

private:
    void onHeaderContextMenuRequested(QPoint const&) const;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_TREE_VIEW_HPP
