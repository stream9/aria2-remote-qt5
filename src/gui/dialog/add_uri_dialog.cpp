#include "add_uri_dialog.hpp"

#include <cassert>

#include <QBoxLayout>
#include <QDialogButtonBox>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QUrl>

namespace aria2_remote {

AddUriDialog::
AddUriDialog(QWidget& parent)
    : AddDialog { parent }
    , m_uri { make_qmanaged<QLineEdit>(this) }
{
    auto& layout = to_ref(static_cast<QVBoxLayout*>(this->layout()));

    auto layout2 = make_qmanaged<QHBoxLayout>();
    auto label = make_qmanaged<QLabel>("URI:", this);
    layout2->addWidget(label.get());
    layout2->addWidget(m_uri.get());

    layout.insertLayout(0, layout2.get());

    this->connect(m_uri, &QLineEdit::textEdited,
                  this,  &AddUriDialog::validateUri);

    this->setWindowTitle("Add URI");

    m_uri->setFocus();
}

std::vector<QString> AddUriDialog::
uris() const
{
    auto results = AddDialog::uris();

    results.insert(results.begin(), m_uri->text());

    return results;
}

void AddUriDialog::
validateUri(QString const& uri_)
{
    auto& buttonBox = this->buttonBox();
    auto& ok = to_ref(buttonBox.button(QDialogButtonBox::Ok));

    QUrl const uri { uri_ };

    ok.setEnabled(uri.isValid());
}

} // namespace aria2_remote
