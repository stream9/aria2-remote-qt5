#ifndef ARIA2_REMOTE_HANDLE_OPTION_DIALOG_HPP
#define ARIA2_REMOTE_HANDLE_OPTION_DIALOG_HPP

#include "option_dialog/option_dialog_base.hpp"

#include <memory>

class QWidget;

namespace aria2_remote {

class Options;

namespace option_dialog {

class Items;

} // namespace option_dialog

class DownloadHandle;

class HandleOptionDialog : public OptionDialogBase
{
    Q_OBJECT
public:
    HandleOptionDialog(DownloadHandle&, QWidget& parent);
    ~HandleOptionDialog();

private:
    Items& items() override { return *m_items; }

    Q_SLOT void onOptionUpdated();

private:
    std::unique_ptr<Options> m_options; // non-null
    std::unique_ptr<option_dialog::Items> m_items; // non-null
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_HANDLE_OPTION_DIALOG_HPP
