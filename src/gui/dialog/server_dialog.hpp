#ifndef ARIA2_REMOTE_SERVER_DIALOG_HPP
#define ARIA2_REMOTE_SERVER_DIALOG_HPP

#include "utility/pointer.hpp"

#include <QDialog>

class QBoxLayout;
class QDialogButtonBox;
class QLayout;
class QLineEdit;
class QWidget;

namespace aria2_remote {

class Server;
class Servers;
class LineEdit;

class ServerDialog : public QDialog
{
    Q_OBJECT
public:
    ServerDialog(QWidget& parent, Servers&);

    QString name() const;
    QString host() const;
    uint16_t port() const;
    QString token() const;

    void setServer(Server const&);

private:
    void createForm(QBoxLayout&);
    void createButtonBox(QBoxLayout&);

    Q_SLOT void updateWidgets();

private:
    Servers& m_servers;

    qmanaged_ptr<LineEdit> m_name;
    qmanaged_ptr<LineEdit> m_host;
    qmanaged_ptr<LineEdit> m_port;
    qmanaged_ptr<QLineEdit> m_token;
    qmanaged_ptr<QDialogButtonBox> m_buttons;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_SERVER_DIALOG_HPP
