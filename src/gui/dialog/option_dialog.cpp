#include "option_dialog.hpp"

#include "option_dialog/item.hpp"
#include "option_dialog/options_filter_model.hpp"
#include "option_dialog/options_model.hpp"
#include "profile_combo_box.hpp"
#include "profile_dialog.hpp"

#include "core/aria2_remote.hpp"
#include "core/profile.hpp"
#include "core/settings.hpp"
#include "gui/tree_view.hpp"

#include <cassert>

#include <QAction>
#include <QBoxLayout>
#include <QCheckBox>
#include <QComboBox>
#include <QDialogButtonBox>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QTreeView>
#include <QWidget>

#include <QDebug>

namespace aria2_remote {

OptionDialog::
OptionDialog(Aria2Remote& aria2, QWidget& parent)
    : OptionDialogBase { aria2, parent }
    , m_profile { make_qmanaged<ProfileComboBox>(*this, true) }
{
    this->bottomPanel().insertWidget(0, m_profile.get(), 1);

    this->connect(m_profile.get(), &ProfileComboBox::profileChanged,
                  this,            &OptionDialog::updateItems);

    updateItems();
}

OptionDialog::~OptionDialog() = default;

void OptionDialog::
updateItems()
{
    using namespace option_dialog;

    if (m_profile->isGlobal()) {
        auto& options = this->aria2().globalOptions();
        m_items = std::make_unique<Items>(options, this->database(), true);
        assert(m_items);
    }
    else {
        auto const& name = m_profile->currentText();
        m_profileOptions = std::make_unique<Profile>(name);
        assert(m_profileOptions);

        m_items = std::make_unique<Items>(
                                *m_profileOptions, this->database(), false);
        assert(m_items);
    }

    this->connect(m_items.get(), &Items::edited,
                  this,          &OptionDialog::onItemEdited);

    this->updateModel();
}

} // namespace aria2_remote
