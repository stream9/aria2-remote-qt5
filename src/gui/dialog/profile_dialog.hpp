#ifndef ARIA2_REMOTE_PROFILE_DIALOG_HPP
#define ARIA2_REMOTE_PROFILE_DIALOG_HPP

#include "utility/pointer.hpp"

#include <QDialog>

class QBoxLayout;
class QDialogButtonBox;
class QLayout;
class QListWidget;
class QPushButton;
class QWidget;

namespace aria2_remote {

class ProfileDialog : public QDialog
{
    Q_OBJECT
public:
    ProfileDialog(QWidget& parent);
    ~ProfileDialog();

private:
    void createTopPanel(QBoxLayout&);
    void createSideButtons(QBoxLayout&);
    void createBottomButtonBox(QBoxLayout&);

    void load();
    void save() const;

    Q_SLOT void addProfile();
    Q_SLOT void removeProfile();
    Q_SLOT void moveUp();
    Q_SLOT void moveDown();
    Q_SLOT void updateWidgets();
    Q_SLOT void onAccepted();
    Q_SLOT void onEdited();

private:
    qmanaged_ptr<QListWidget> m_profiles;
    qmanaged_ptr<QPushButton> m_add;
    qmanaged_ptr<QPushButton> m_remove;
    qmanaged_ptr<QPushButton> m_up;
    qmanaged_ptr<QPushButton> m_down;
    qmanaged_ptr<QDialogButtonBox> m_buttonBox;
    bool m_edited = false;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_PROFILE_DIALOG_HPP
