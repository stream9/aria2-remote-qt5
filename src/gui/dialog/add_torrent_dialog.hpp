#ifndef ARIA2_REMOTE_ADD_TORRENT_DIALOG_HPP
#define ARIA2_REMOTE_ADD_TORRENT_DIALOG_HPP

#include "add_dialog.hpp"
#include "utility/pointer.hpp"

class QBoxLayout;
class QLayout;
class QLineEdit;
class QWidget;

namespace aria2_remote {

class AddTorrentDialog : public AddDialog
{
    Q_OBJECT
public:
    AddTorrentDialog(QWidget& parent);

    QString fileName() const;

private:
    void createTorrentWidget(QBoxLayout&);

    Q_SLOT void validateTorrent();
    Q_SLOT void showFileOpenDialog();

private:
    qmanaged_ptr<QLineEdit> m_torrent;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_ADD_TORRENT_DIALOG_HPP
