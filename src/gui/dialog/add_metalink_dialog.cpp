#include "add_metalink_dialog.hpp"

#include <cassert>
#include <vector>

#include <QBoxLayout>
#include <QDialogButtonBox>
#include <QFileDialog>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QToolButton>

#include <QDebug>

namespace aria2_remote {

AddMetalinkDialog::
AddMetalinkDialog(QWidget& parent)
    : AddDialog { parent }
    , m_metalink { make_qmanaged<QLineEdit>(this) }
{
    auto& layout = to_ref(static_cast<QVBoxLayout*>(this->layout()));

    createMetalinkWidget(layout);

    this->setWindowTitle("Add Metalink");

    this->additionalUriButton().setVisible(false);

    m_metalink->setFocus();
}

AddMetalinkDialog::~AddMetalinkDialog() = default;

QString AddMetalinkDialog::
fileName() const
{
    return m_metalink->text();
}

void AddMetalinkDialog::
createMetalinkWidget(QBoxLayout& parent)
{
    auto layout = make_qmanaged<QHBoxLayout>();

    auto label = make_qmanaged<QLabel>("Filename:", this);
    layout->addWidget(label.get());

    auto edit = make_qmanaged<QLineEdit>(this);
    layout->addWidget(edit.get());

    this->connect(edit.get(), &QLineEdit::textChanged,
                  this,       &AddMetalinkDialog::validateMetalink);

    auto button = make_qmanaged<QToolButton>(this);
    button->setText("...");
    layout->addWidget(button.get());

    this->connect(button.get(), &QToolButton::clicked,
                  this,         &AddMetalinkDialog::showFileOpenDialog);

    parent.insertLayout(0, layout.get());
}

void AddMetalinkDialog::
validateMetalink()
{
    auto& buttonBox = this->buttonBox();
    auto& ok = to_ref(buttonBox.button(QDialogButtonBox::Ok));

    ok.setEnabled(!m_metalink->text().isEmpty());
}

void AddMetalinkDialog::
showFileOpenDialog()
{
    auto const fileName = QFileDialog::getOpenFileName(
            this,
            "Open Metalink File",
            {},
            "Metalink File (*.meta4 *.metalink)");

    m_metalink->setText(fileName);
}

} // namespace aria2_remote
