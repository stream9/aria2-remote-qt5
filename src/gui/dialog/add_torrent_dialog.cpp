#include "add_torrent_dialog.hpp"

#include <cassert>
#include <vector>

#include <QBoxLayout>
#include <QDialogButtonBox>
#include <QFileDialog>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QToolButton>

#include <QDebug>

namespace aria2_remote {

AddTorrentDialog::
AddTorrentDialog(QWidget& parent)
    : AddDialog { parent }
    , m_torrent { make_qmanaged<QLineEdit>(this) }
{
    auto& layout = to_ref(static_cast<QVBoxLayout*>(this->layout()));

    createTorrentWidget(layout);

    this->setWindowTitle("Add Torrent");

    m_torrent->setFocus();
}

QString AddTorrentDialog::
fileName() const
{
    return m_torrent->text();
}

void AddTorrentDialog::
createTorrentWidget(QBoxLayout& parent)
{
    auto layout = make_qmanaged<QHBoxLayout>();

    auto label = make_qmanaged<QLabel>("Filename:" , this);
    layout->addWidget(label.get());

    layout->addWidget(m_torrent.get());

    this->connect(m_torrent.get(), &QLineEdit::textChanged,
                  this,            &AddTorrentDialog::validateTorrent);

    auto button = make_qmanaged<QToolButton>(this);
    button->setText("...");
    layout->addWidget(button.get());

    this->connect(button.get(), &QToolButton::clicked,
                  this,         &AddTorrentDialog::showFileOpenDialog);

    parent.insertLayout(0, layout.get());
}

void AddTorrentDialog::
validateTorrent()
{
    auto& buttonBox = this->buttonBox();
    auto& ok = to_ref(buttonBox.button(QDialogButtonBox::Ok));

    ok.setEnabled(!m_torrent->text().isEmpty());
}

void AddTorrentDialog::
showFileOpenDialog()
{
    auto const fileName = QFileDialog::getOpenFileName(
            this,
            "Open Torrent File",
            {},
            "Torrent File (*.torrent)");

    m_torrent->setText(fileName);
}

} // namespace aria2_remote
