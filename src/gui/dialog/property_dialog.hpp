#ifndef ARIA2_REMOTE_PROPERTY_DIALOG_HPP
#define ARIA2_REMOTE_PROPERTY_DIALOG_HPP

#include "utility/pointer.hpp"

#include <QDialog>

class QDialogButtonBox;
class QTabWidget;
class QWidget;

namespace aria2_remote {

class PropertyDialog : public QDialog
{
public:
    PropertyDialog();

private:
    qmanaged_ptr<QTabWidget> m_tab;
    qmanaged_ptr<QDialogButtonBox> m_buttonBox;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_PROPERTY_DIALOG_HPP
