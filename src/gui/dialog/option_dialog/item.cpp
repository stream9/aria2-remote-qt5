#include "item.hpp"

#include "core/options.hpp"

#include <algorithm>

#include <QDebug>
#include <QJsonObject>
#include <QSqlDatabase>
#include <QSqlError>
#include <QSqlQuery>
#include <QVariant>

namespace aria2_remote::option_dialog {

// Item

Item::
Item(QString const& name
   , QString const& category
   , QString const& value
   , QString const& default_
   , bool const editable)
    : m_name { name }
    , m_category { category }
    , m_before { value }
    , m_after { m_before }
    , m_default { default_ }
    , m_editable { editable }
{}

void Item::
setValue(QString const& value)
{
    m_after = value;
}

// Items

Items::
Items(Options& options, QSqlDatabase& db, bool const global)
    : m_options { options }
{
    QSqlQuery query { db };
    auto success = query.exec(
        "SELECT name, category, `default`, "
        "       change_option, "
        "       change_global_option "
        "  FROM option "
    );
    if (!success) {
        qDebug() << query.lastError().text();
    }

    while (query.next()) {
        auto const& name = query.value("name").toString();
        auto const& category = query.value("category").toString();
        auto const& default_ = query.value("default").toString();
        auto const editable = global
            ? query.value("change_global_option").toBool()
            : query.value("change_option").toBool();

        auto* const value = m_options.value(name);

        m_items.emplace_back(name, category,
                value ? *value : default_,
                default_, editable);
    }
}

bool Items::
isEdited() const
{
    return std::any_of(m_items.begin(), m_items.end(),
        [&](auto const& item) {
            return item.isEdited();
        });
}

void Items::
setValue(size_t const row, QString const& value)
{
    m_items.at(row).setValue(value);
    Q_EMIT edited();
}

void Items::
commitChange()
{
    QJsonObject options;

    for (auto const& item: m_items) {
        if (item.isEdited()) {
            auto const& name = item.name();
            auto const& value = item.value();

            options.insert(name, value);
        }
    }

    m_options.setValues(options);
}

} // namespace aria2_remote::option_dialog
