#ifndef ARIA2_REMOTE_OPTIONS_MODEL_HPP
#define ARIA2_REMOTE_OPTIONS_MODEL_HPP

#include <QAbstractItemModel>

class QModelIndex;
class QVariant;

namespace aria2_remote {

namespace option_dialog {

class Items;

} // namespace option_dialog

class OptionsModel : public QAbstractItemModel
{
    Q_OBJECT

    enum Column { Name = 0, Category, Default, Value, Count };
public:
    OptionsModel(option_dialog::Items&);

private:
    QVariant getDisplayData(QModelIndex const&) const;
    QVariant getEditData(QModelIndex const&) const;
    QVariant getForegroundData(QModelIndex const&) const;
    QVariant getFontData(QModelIndex const&) const;

    // override QAbstractItemModel
    QModelIndex index(int row, int column,
                      QModelIndex const& parent = {}) const override;
    QModelIndex parent(QModelIndex const&) const override;
    int rowCount(QModelIndex const& parent = {}) const override;
    int columnCount(QModelIndex const& parent = {}) const override;
    QVariant data(QModelIndex const&, int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation, int role = Qt::DisplayRole) const override;
    Qt::ItemFlags flags(QModelIndex const& index) const override;
    bool setData(QModelIndex const& index, QVariant const& value, int role = Qt::EditRole) override;

private:
    option_dialog::Items& m_items;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_OPTIONS_MODEL_HPP

