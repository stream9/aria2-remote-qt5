#ifndef AREA2_REMOTE_OPTIONS_FILTER_MODEL_HPP
#define AREA2_REMOTE_OPTIONS_FILTER_MODEL_HPP

#include <QSortFilterProxyModel>
#include <QString>

class QModelIndex;

namespace aria2_remote {

class OptionsFilterModel : public QSortFilterProxyModel
{
public:
    OptionsFilterModel() = default;

    void setCategory(QString const& category);
    void setFilter(QString const&);
    void setEditableOnly(bool);
    void setModifiedOnly(bool);

private:
    // override QSortFilterProxyModel
    bool filterAcceptsRow(int srcRow, QModelIndex const& srcParent) const override;

private:
    QString m_category;
    QString m_filter;
    bool m_editableOnly = false;
    bool m_modifiedOnly = false;
};

} // namespace aria2_remote

#endif // AREA2_REMOTE_OPTIONS_FILTER_MODEL_HPP
