#ifndef ARIA2_REMOTE_ADD_METALINK_DIALOG_HPP
#define ARIA2_REMOTE_ADD_METALINK_DIALOG_HPP

#include "add_dialog.hpp"

#include "utility/pointer.hpp"

class QLayout;
class QLineEdit;
class QWidget;

namespace aria2_remote {

class AddMetalinkDialog : public AddDialog
{
    Q_OBJECT
public:
    AddMetalinkDialog(QWidget& parent);
    ~AddMetalinkDialog() override;

    QString fileName() const;

private:
    void createMetalinkWidget(QBoxLayout&);

    Q_SLOT void validateMetalink();
    Q_SLOT void showFileOpenDialog();

private:
    qmanaged_ptr<QLineEdit> m_metalink;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_ADD_METALINK_DIALOG_HPP
