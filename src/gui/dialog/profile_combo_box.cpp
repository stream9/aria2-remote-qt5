#include "profile_combo_box.hpp"

#include "core/application.hpp"
#include "core/settings.hpp"
#include "gui/dialog/profile_dialog.hpp"

#include <QString>
#include <QTimer>

namespace aria2_remote {

ProfileComboBox::
ProfileComboBox(QWidget& parent, bool const includeGlobal/*=false*/)
    : QComboBox { &parent }
    , m_includeGlobal { includeGlobal }
{
    this->connect(this, qOverload<int const>(&QComboBox::activated),
                  this, &ProfileComboBox::onActivated);
    load();
}

bool ProfileComboBox::
isGlobal() const
{
    if (!m_includeGlobal) return false;

    return this->currentIndex() == 0;
}

void ProfileComboBox::
load()
{
    if (m_includeGlobal) {
        this->addItem("Global");
    }

    auto& settings = gApp().settings();
    auto const& profiles = settings.profiles();

    for (auto const& name: profiles) {
        this->addItem(name);
    }

    this->addItem("Edit Profiles");
}

void ProfileComboBox::
reload()
{
    this->clear();
    load();
}

int ProfileComboBox::
lastIndex() const
{
    return this->count() - 1;
}

void ProfileComboBox::
onActivated(int const index)
{
    if (index == m_current) return;

    if (index == lastIndex()) { // last item - "Edit Profile"
        ProfileDialog dialog { *this };

        auto const result = dialog.exec();
        if (result == QDialog::Accepted) {
            reload();
        }

        if (m_current == lastIndex()) {
            m_current = 0;
            this->setCurrentIndex(0);

            Q_EMIT this->profileChanged(this->itemText(index));
        }
        else {
            this->setCurrentIndex(m_current);
        }
    }
    else {
        m_current = index;

        Q_EMIT this->profileChanged(this->itemText(index));
    }
}

} // namespace aria2_remote
