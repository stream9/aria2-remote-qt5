#include "name_column_delegate.hpp"

#include <QStyleOptionViewItem>

namespace aria2_remote {

NameColumnDelegate::
NameColumnDelegate(QObject* const parent/*= nullptr*/)
    : QStyledItemDelegate { parent }
{}

void NameColumnDelegate::
paint(QPainter* const painter,
      QStyleOptionViewItem const& option,
      QModelIndex const& index) const
{
    auto opt = option;
    opt.textElideMode = Qt::ElideLeft;

    QStyledItemDelegate::paint(painter, opt, index);
}

} // namespace aria2_remote
