#include "add_metalink.hpp"

#include "core/application.hpp"
#include "core/aria2_remote.hpp"
#include "core/profile.hpp"
#include "core/rpc/aria2_rpc.hpp"
#include "gui/dialog/add_metalink_dialog.hpp"
#include "gui/icons.hpp"
#include "json_rpc/response.hpp"
#include "utility/pointer.hpp"

#include <cassert>

#include <QFile>
#include <QJsonArray>
#include <QJsonObject>
#include <QApplication>
#include <QFileDialog>
#include <QMessageBox>

#include <QDebug>

namespace aria2_remote::action {

AddMetalink::
AddMetalink(ActionManager& manager, Application& app)
    : Base { manager }
    , m_application { app }
{
    this->setText("&Add Metalink");
    this->setIcon(icons::addMetalink());

    this->connect(&m_application, &Application::aria2Changed,
                  this,           &AddMetalink::update);
}

void AddMetalink::
run()
{
    auto* const aria2 = m_application.aria2();
    if (!aria2) return;

    auto& window = to_ref(QApplication::activeWindow());

    AddMetalinkDialog dialog { window };
    if (dialog.exec() == QDialog::Rejected) return;

    QFile file { dialog.fileName() };
    if (!file.open(QIODevice::ReadOnly)) {
        QMessageBox::critical(&window, "Error", "Can't open file");
        return;
    }

    auto const bytes = file.readAll();

    Profile const profile { dialog.profile() };

    aria2->rpc().addMetalink(bytes.toBase64(), profile.toJson(),
        [&](auto const& res) {
            assert(res); (void)res; //TODO properly

            assert(res.value().isArray());
        });
}

void AddMetalink::
update()
{
    if (m_application.aria2()) {
        this->setEnabled(true);
    }
    else {
        this->setEnabled(false);
    }
}

} // namespace aria2_remote::action
