#ifndef ARIA2_REMOTE_ACTION_PAUSE_HPP
#define ARIA2_REMOTE_ACTION_PAUSE_HPP

#include "base.hpp"

#include <QTimer>

#include <memory>
#include <vector>

namespace aria2_remote {

class ActionManager;
class Application;
class DownloadHandle;

namespace action {

class Pause : public Base
{
    Q_OBJECT

    using Selection = std::vector<std::weak_ptr<DownloadHandle>>;
public:
    Pause(ActionManager&, Application&);

private:
    void run() override;
    void update() override;

    void updateSelection();

private:
    Application& m_application;
    Selection m_selection;
    QTimer m_updateTimer;
};

}} // namespace aria2_remote::action

#endif // ARIA2_REMOTE_ACTION_PAUSE_HPP

