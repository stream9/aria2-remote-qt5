#ifndef ARIA2_REMOTE_ACTION_COPY_URI_HPP
#define ARIA2_REMOTE_ACTION_COPY_URI_HPP

#include "base.hpp"

#include "core/fwd/action_manager.hpp"
#include "core/fwd/application.hpp"

#include <QTimer>

#include <memory>
#include <vector>

namespace aria2_remote::action {

class CopyUri : public Base
{
    Q_OBJECT
public:
    CopyUri(ActionManager&, Application&);

private:
    void run() override;
    void update() override;

    void updateSelection();

private:
    Application& m_application;
};

} // namespace aria2_remote::action

#endif // ARIA2_REMOTE_ACTION_COPY_URI_HPP
