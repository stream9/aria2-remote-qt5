#include "move_to_top.hpp"

#include "core/application.hpp"
#include "core/download_handle.hpp"
#include "core/download_status.hpp"
#include "gui/icons.hpp"

#include <algorithm>
#include <cassert>

namespace aria2_remote::action {

MoveToTop::
MoveToTop(ActionManager& manager, Application& app)
    : Base { manager }
    , m_application { app }
{
    this->setText("&Move to Top");
    this->setIcon(icons::moveTop());
}

void MoveToTop::
run()
{
    for (DownloadHandle& handle: m_application.selection()) {
        handle.changePosition(0, OffsetMode::Set);
    }
}

static bool
isInWaitingQueue(DownloadHandle const& handle)
{
    return handle.status() == DownloadStatus::Waiting
        || handle.status() == DownloadStatus::Paused;
}

void MoveToTop::
update()
{
    auto& selection = m_application.selection();

    bool enable = false;

    if (!selection.empty()) {
        enable = std::all_of(selection.begin(), selection.end(),
            [&](DownloadHandle const& handle) {
                return isInWaitingQueue(handle);
            });
    }

    this->setEnabled(enable);
}

} // namespace aria2_remote::action
