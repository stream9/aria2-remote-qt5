#ifndef ARIA2_REMOTE_QT5_GUI_ACTION_REMOVE_COMPLETED_HPP
#define ARIA2_REMOTE_QT5_GUI_ACTION_REMOVE_COMPLETED_HPP

#include "base.hpp"

#include "core/fwd/action_manager.hpp"
#include "core/fwd/application.hpp"
#include "core/fwd/aria2_remote.hpp"

namespace aria2_remote::action {

class RemoveCompleted : public Base
{
    Q_OBJECT
public:
    RemoveCompleted(ActionManager&, Application&);

private:
    void run() override;
    void update() override;

    Q_SLOT void onAria2Changed(Aria2Remote*);

private:
    Application& m_application;
};

} // namespace aria2_remote::action

#endif // ARIA2_REMOTE_QT5_GUI_ACTION_REMOVE_COMPLETED_HPP
