#include "move_up.hpp"

#include "core/application.hpp"
#include "core/download_handle.hpp"
#include "core/download_status.hpp"
#include "gui/icons.hpp"

#include <algorithm>
#include <cassert>

namespace aria2_remote::action {

MoveUp::
MoveUp(ActionManager& manager, Application& app)
    : Base { manager }
    , m_application { app }
{
    this->setText("&Move Up");
    this->setIcon(icons::moveUp());
}

void MoveUp::
run()
{
    for (DownloadHandle& handle: m_application.selection()) {
        handle.changePosition(-1, OffsetMode::Current);
    }
}

static bool
isInWaitingQueue(DownloadHandle const& handle)
{
    return handle.status() == DownloadStatus::Waiting
        || handle.status() == DownloadStatus::Paused;
}

void MoveUp::
update()
{
    auto& selection = m_application.selection();

    bool enable = false;

    if (!selection.empty()) {
        enable = std::all_of(selection.begin(), selection.end(),
            [&](DownloadHandle const& handle) {
                return isInWaitingQueue(handle);
            });
    }

    this->setEnabled(enable);
}

} // namespace aria2_remote::action
