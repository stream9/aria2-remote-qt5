#ifndef ARIA2_REMOTE_GUI_ACTION_MOVE_TO_BOTTOM_HPP
#define ARIA2_REMOTE_GUI_ACTION_MOVE_TO_BOTTOM_HPP

#include "base.hpp"

namespace aria2_remote {

class ActionManager;
class Application;

namespace action {

class MoveToBottom : public Base
{
public:
    MoveToBottom(ActionManager&, Application&);

private:
    void run() override;
    void update() override;

private:
    Application& m_application;
};

}} // namespace aria2_remote::action

#endif // ARIA2_REMOTE_GUI_ACTION_MOVE_TO_BOTTOM_HPP
