#include "remove_server.hpp"

#include "core/application.hpp"
#include "gui/icons.hpp"
#include "core/server.hpp"

#include <cassert>

#include <QApplication>
#include <QMessageBox>

namespace aria2_remote::action {

RemoveServer::
RemoveServer(ActionManager& manager, Application& app)
    : Base { manager }
    , m_application { app }
{
    this->setText("&Remove Server");
    this->setIcon(QIcon::fromTheme("network-server"));

    this->connect(&m_application, &Application::currentServerChanged,
                  this,           &RemoveServer::update);
    update();
}

void RemoveServer::
run()
{
    auto* const current = m_application.currentServer();
    if (!current) return;

    QString const fmt { "Do you really want remove server \"%1\"?" };

    auto const result = QMessageBox::question(
        QApplication::activeWindow(),
        "Remove Download",
        fmt.arg(current->name())
    );

    if (result == QMessageBox::Yes) {
        m_application.servers().remove(*current);
    }
}

void RemoveServer::
update()
{
    this->setEnabled(m_application.currentServer());
}

} // namespace aria2_remote::action
