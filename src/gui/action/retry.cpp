#include "retry.hpp"

#include "core/application.hpp"
#include "core/aria2_remote.hpp"
#include "core/download_handle.hpp"
#include "core/rpc/aria2_rpc.hpp"
#include "gui/icons.hpp"
#include "json_rpc/response.hpp"
#include "utility/pointer.hpp"

#include <cassert>

#include <QJsonArray>

namespace aria2_remote::action {

static bool
shouldEnabled(Application& app)
{
    if (!app.aria2()) {
        return false;
    }

    auto const& selection = app.selection();
    if (selection.empty()) {
        return false;
    }

    return true;
}

/*
 * Retry
 */
Retry::
Retry(ActionManager& manager, Application& app)
    : Base { manager }
    , m_application { app }
{
    this->setText("&Retry");
    this->setIcon(icons::retry());

    this->connect(&m_application, &Application::aria2Changed,
                  this,           &Retry::update);
}

void Retry::
run()
{
    auto* const aria2 = m_application.aria2();
    if (!aria2) return;

    for (DownloadHandle& handle: m_application.selection()) {
        auto const& files = handle.files();
        assert(!files.empty());
        auto const& aFile = files.front();

        auto const& fileUris = aFile.uris();
        assert(!fileUris.empty());
        auto const& aFileUri = fileUris.front().uri();

        QJsonArray uris;
        uris.push_back(aFileUri);

        aria2->rpc().addUri(uris,
            [&](auto&& res) {
                assert(res); (void)res; //TODO properly
                assert(res.value().isString());
            } );
    }
}

void Retry::
update()
{
    this->setEnabled(shouldEnabled(m_application));
}

} // namespace aria2_remote::action
