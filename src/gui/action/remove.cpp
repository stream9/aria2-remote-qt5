#include "remove.hpp"

#include "core/application.hpp"
#include "core/download_handle.hpp"
#include "gui/icons.hpp"

#include <cassert>

#include <QKeySequence>
#include <QApplication>
#include <QMessageBox>

namespace aria2_remote::action {

Remove::
Remove(ActionManager& manager, Application& app)
    : Base { manager }
    , m_application { app }
{
    this->setText("&Remove");
    this->setIcon(icons::remove());
    this->setShortcut(QKeySequence::Delete);
}

void Remove::
run()
{
    auto const& selection = m_application.selection();
    if (selection.empty()) return;

    static auto const singleMessage =
            "Do you really want to remove this download?";

    static auto const pluralMessage =
            "Do you really want to remove these downloads?";

    auto const result = QMessageBox::question(
        QApplication::activeWindow(),
        "Remove Download",
        selection.size() == 1 ? singleMessage : pluralMessage
    );

    if (result == QMessageBox::Yes) {
        for (DownloadHandle& handle: selection) {
            handle.remove();
        }
    }
}

void Remove::
update()
{
    if (!m_application.selection().empty()) {
        this->setEnabled(true);
    }
    else {
        this->setEnabled(false);
    }
}

} // namespace aria2_remote::action
