#ifndef ARIA2_REMOTE_ACTION_BASE_HPP
#define ARIA2_REMOTE_ACTION_BASE_HPP

#include <QAction>

namespace aria2_remote {

class ActionManager;

namespace action {

class Base: public QAction
{
    Q_OBJECT
public:
    Base(ActionManager&);

    ActionManager& manager() { return m_manager; }

private:
    Q_SLOT virtual void run() = 0;
    Q_SLOT virtual void update() = 0;

private:
    ActionManager& m_manager;
};

}} // namespace aria2_remote::action

#endif // ARIA2_REMOTE_ACTION_BASE_HPP
