#include "quit.hpp"

#include "gui/icons.hpp"

#include <cassert>

#include <QCoreApplication>

namespace aria2_remote::action {

Quit::
Quit(ActionManager& manager)
    : Base { manager }
{
    this->setText("&Quit");
    this->setIcon(icons::quit());
    this->setShortcut(QKeySequence::Quit);
}

void Quit::
run()
{
    QCoreApplication::exit();
}

void Quit::
update()
{
}

} // namespace aria2_remote::action
