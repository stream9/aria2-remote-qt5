#include "connect.hpp"

#include "core/application.hpp"
#include "gui/icons.hpp"
#include "core/server.hpp"

#include <cassert>

namespace aria2_remote::action {

Connect::
Connect(ActionManager& manager, Application& app)
    : Base { manager }
    , m_application { app }
{
    this->connect(&m_application, &Application::currentServerChanged,
                  this,           &Connect::onCurrentChanged);
    update();
}

void Connect::
run()
{
    if (m_current == nullptr) return;

    if (isConnected()) {
        m_current->disconnect();
    }
    else {
        m_current->connect();
    }
}

void Connect::
update()
{
    this->setEnabled(m_current != nullptr);
    this->setText(isConnected() ? "Dis&connect" : "&Connect");
    this->setIcon(isConnected() ? icons::disconnect()
                                : icons::connect());
}

bool Connect::
isConnected() const
{
    return m_current && m_current->isConnected();
}

void Connect::
onCurrentChanged()
{
    if (m_current) {
        m_current->disconnect(this);
    }

    m_current = m_application.currentServer();

    if (m_current) {
        this->connect(m_current, &Server::connected,
                      this,      &Connect::update);
        this->connect(m_current, &Server::disconnected,
                      this,      &Connect::update);
    }

    update();
}

} // namespace aria2_remote::action
