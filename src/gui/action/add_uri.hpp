#ifndef ARIA2_REMOTE_ACTION_ADD_URI_HPP
#define ARIA2_REMOTE_ACTION_ADD_URI_HPP

#include "base.hpp"

namespace aria2_remote {

class ActionManager;
class Application;

namespace action {

class AddUri : public Base
{
public:
    AddUri(ActionManager&, Application&);

private:
    void run() override;
    void update() override;

private:
    Application& m_application;
};

}} // namespace aria2_remote::action

#endif // ARIA2_REMOTE_ACTION_ADD_URI_HPP
