#include "add_torrent.hpp"

#include "core/application.hpp"
#include "core/aria2_remote.hpp"
#include "core/profile.hpp"
#include "core/rpc/aria2_rpc.hpp"
#include "gui/dialog/add_torrent_dialog.hpp"
#include "gui/icons.hpp"
#include "json_rpc/response.hpp"
#include "utility/pointer.hpp"

#include <cassert>

#include <QFile>
#include <QJsonArray>
#include <QJsonObject>
#include <QApplication>
#include <QFileDialog>
#include <QMessageBox>

#include <QDebug>

namespace aria2_remote::action {

AddTorrent::
AddTorrent(ActionManager& manager, Application& app)
    : Base { manager }
    , m_application { app }
{
    this->setText("&Add Torrent");
    this->setIcon(icons::addTorrent());

    this->connect(&m_application, &Application::aria2Changed,
                  this,           &AddTorrent::update);
}

void AddTorrent::
run()
{
    auto* const aria2 = m_application.aria2();
    if (!aria2) return;

    auto& window = to_ref(QApplication::activeWindow());

    AddTorrentDialog dialog { window };
    if (dialog.exec() == QDialog::Rejected) return;

    QFile file { dialog.fileName() };
    if (!file.open(QIODevice::ReadOnly)) {
        QMessageBox::critical(&window, "Error", "Can't open file");
        return;
    }

    auto const bytes = file.readAll();

    QJsonArray uris;
    for (auto const& uri: dialog.uris()) {
        uris.push_back(uri);
    }

    Profile const profile { dialog.profile() };

    aria2->rpc().addTorrent(bytes.toBase64(), uris, profile.toJson(),
        [&](auto const& res) {
            assert(res); (void)res; //TODO properly

            assert(res.value().isString());
        });
}

void AddTorrent::
update()
{
    if (m_application.aria2()) {
        this->setEnabled(true);
    }
    else {
        this->setEnabled(false);
    }
}

} // namespace aria2_remote::action
