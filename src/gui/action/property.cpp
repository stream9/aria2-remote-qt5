#include "property.hpp"

#include "core/application.hpp"
#include "gui/dialog/property_dialog.hpp"
#include "gui/icons.hpp"

#include <cassert>

namespace aria2_remote::action {

Property::
Property(ActionManager& manager, Application& app)
    : Base { manager }
    , m_application { app }
{
    this->setText("&Property");
    this->setIcon(icons::option());
}

void Property::
run()
{
    PropertyDialog dialog;

    dialog.exec();
}

void Property::
update()
{
    if (m_application.currentHandle()) {
        this->setEnabled(true);
    }
    else {
        this->setEnabled(false);
    }
}

} // namespace aria2_remote::action
