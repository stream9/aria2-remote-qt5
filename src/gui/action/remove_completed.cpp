#include "remove_completed.hpp"

#include "core/application.hpp"
#include "core/aria2_remote.hpp"
#include "core/download_handle.hpp"
#include "core/download_handles.hpp"
#include "gui/icons.hpp"

#include <ranges>

namespace aria2_remote::action {

namespace rng = std::ranges;

static bool
shouldEnabled(Application& app)
{
    auto* const aria2 = app.aria2();
    if (!aria2) return false;

    auto const& handles = aria2->stoppedDownloads();
    auto const has_completed = rng::any_of(handles,
        [&](auto&& handlePtr) {
            return handlePtr->status() == DownloadStatus::Value::Complete;
        });

    return has_completed;
}

/*
 * RemoveCompleted
 */
RemoveCompleted::
RemoveCompleted(ActionManager& manager, Application& app)
    : Base { manager }
    , m_application { app }
{
    this->setText("Remove Completed");
    this->setShortcut(Qt::SHIFT | Qt::Key_Delete);
    this->setIcon(icons::remove_completed());

    this->connect(&m_application, &Application::aria2Changed,
                  this,           &RemoveCompleted::onAria2Changed);

    onAria2Changed(m_application.aria2());
}

void RemoveCompleted::
run()
{
    auto* const aria2 = m_application.aria2();
    if (!aria2) return;

    for (auto&& handlePtr: aria2->stoppedDownloads()) {
        if (handlePtr->status() == DownloadStatus::Value::Complete) {
            handlePtr->remove();
        }
    }
}

void RemoveCompleted::
update()
{
    this->setEnabled(shouldEnabled(m_application));
}

void RemoveCompleted::
onAria2Changed(Aria2Remote* const aria2)
{
    if (aria2) {
        this->connect(aria2, &Aria2Remote::stoppedDownloadsChanged,
                      this,  &RemoveCompleted::update);
    }

    update();
}

} // namespace aria2_remote::action
