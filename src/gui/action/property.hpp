#ifndef ARIA2_REMOTE_ACTION_PROPERTY_HPP
#define ARIA2_REMOTE_ACTION_PROPERTY_HPP

#include "base.hpp"

namespace aria2_remote {

class ActionManager;
class Application;

namespace action {

class Property : public Base
{
public:
    Property(ActionManager&, Application&);

private:
    void run() override;
    void update() override;

private:
    Application& m_application;
};

}} // namespace aria2_remote::action

#endif // ARIA2_REMOTE_ACTION_PROPERTY_HPP

