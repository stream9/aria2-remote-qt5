#include "copy_uri.hpp"

#include "core/application.hpp"
#include "core/aria2_remote.hpp"
#include "core/download_handle.hpp"
#include "gui/icons.hpp"
#include "utility/pointer.hpp"

#include <QApplication>
#include <QClipboard>
#include <QString>

#include <boost/container/flat_set.hpp>
#include <boost/container/small_vector.hpp>

namespace aria2_remote::action {

namespace con = boost::container;

static bool
shouldEnabled(Application& app)
{
    if (!app.aria2()) {
        return false;
    }

    auto const& selection = app.selection();
    if (selection.size() != 1) {
        return false;
    }

    return true;
}

static QString
join(con::small_flat_set<QString, 10> const& uris)
{
    QString result;
    bool first = true;

    for (auto const& uri: uris) {
        if (!first) {
            result.push_back('\n');
        }
        else {
            first = false;
        }

        result.append(uri);
    }

    return result;
}

/*
 * CopyUri
 */
CopyUri::
CopyUri(ActionManager& manager, Application& app)
    : Base { manager }
    , m_application { app }
{
    this->setText("&Copy URI");
    this->setShortcut(QKeySequence::Copy);
    this->setIcon(icons::copy());

    this->connect(&m_application, &Application::aria2Changed,
                  this,           &CopyUri::update);
}

void CopyUri::
run()
{
    if (!shouldEnabled(m_application)) return;

    DownloadHandle& handle = m_application.selection().front();

    auto const& files = handle.files();
    assert(!files.empty());
    auto const& aFile = files.front();

    con::small_flat_set<QString, 10> uris;
    for (auto const& fileUri: aFile.uris()) {
        uris.insert(fileUri.uri());
    }

    auto& clipboard = to_ref(qApp->clipboard());
    clipboard.setText(join(uris));
}

void CopyUri::
update()
{
    this->setEnabled(shouldEnabled(m_application));
}

} // namespace aria2_remote::action
