#ifndef ARIA2_REMOTE_QT5_GUI_ACTION_RETRY_HPP
#define ARIA2_REMOTE_QT5_GUI_ACTION_RETRY_HPP

#include "base.hpp"

#include "core/fwd/application.hpp"
#include "core/fwd/action_manager.hpp"

namespace aria2_remote::action {

class Retry : public Base
{
public:
    Retry(ActionManager&, Application&);

private:
    void run() override;
    void update() override;

private:
    Application& m_application;
};

} // namespace aria2_remote::action

#endif // ARIA2_REMOTE_QT5_GUI_ACTION_RETRY_HPP
