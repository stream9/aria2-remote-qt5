#include "handle_option.hpp"

#include "core/application.hpp"
#include "gui/dialog/handle_option_dialog.hpp"
#include "utility/pointer.hpp"

#include <cassert>

#include <QApplication>

namespace aria2_remote::action {

HandleOption::
HandleOption(ActionManager& manager, Application& app)
    : Base { manager }
    , m_application { app }
{
    this->setText("&Handle Option");
    this->setIcon(QIcon::fromTheme("document-properties"));
}

void HandleOption::
run()
{
    auto* const handle = m_application.currentHandle();
    if (!handle) return;

    auto& window = to_ref(QApplication::activeWindow());

    HandleOptionDialog dialog { *handle, window };

    dialog.exec();
}

void HandleOption::
update()
{
    if (m_application.currentHandle()) {
        this->setEnabled(true);
    }
    else {
        this->setEnabled(false);
    }
}

} // namespace aria2_remote::action
