#ifndef ARIA2_REMOTE_ACTION_CONNECT_HPP
#define ARIA2_REMOTE_ACTION_CONNECT_HPP

#include "base.hpp"

namespace aria2_remote {

class ActionManager;
class Application;
class Server;

namespace action {

class Connect : public Base
{
    Q_OBJECT
public:
    Connect(ActionManager&, Application&);

private:
    void run() override;
    void update() override;

    bool isConnected() const;

    Q_SLOT void onCurrentChanged();

private:
    Application& m_application;
    Server* m_current = nullptr; // nullable
};

}} // namespace aria2_remote::action

#endif // ARIA2_REMOTE_ACTION_CONNECT_HPP
