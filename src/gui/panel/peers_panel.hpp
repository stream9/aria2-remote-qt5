#ifndef ARIA2_REMOTE_PEERS_PANEL_HPP
#define ARIA2_REMOTE_PEERS_PANEL_HPP

#include "utility/pointer.hpp"

#include <memory>

#include <QSortFilterProxyModel>
#include <QWidget>

class QAbstractItemModel;

namespace aria2_remote {

class DownloadHandle;
class PeersModel;
class TreeView;

class PeersPanel : public QWidget
{
    Q_OBJECT
public:
    PeersPanel();
    ~PeersPanel() override;

private:
    Q_SLOT void readSettings();
    Q_SLOT void writeSettings() const;

    Q_SLOT void setDownloadHandle(DownloadHandle*);
    Q_SLOT void onHandleDestroyed();

private:
    DownloadHandle* m_handle = nullptr;

    qmanaged_ptr<TreeView> m_uris;

    std::unique_ptr<PeersModel> m_model; // non-null
    QSortFilterProxyModel m_sortModel;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_PEERS_PANEL_HPP
