#include "download_uris_model.hpp"

#include "core/download_handle.hpp"

namespace aria2_remote {

DownloadUrisModel::
DownloadUrisModel(DownloadHandle* const handle/*= nullptr*/)
    : UrisModel { handle }
{
    if (handle) {
        handle->setUpdateUris(true);

        this->connect(handle, &DownloadHandle::urisChanged,
                      this,   &DownloadUrisModel::onUrisChanged);
    }
}

DownloadUrisModel::
~DownloadUrisModel()
{
    if (this->handle()) {
        this->handle()->setUpdateUris(false);
    }

}

UrisModel::UriDatas const& DownloadUrisModel::
uris() const
{
    static UriDatas const empty;

    if (this->handle()) {
        return this->handle()->uris();
    }
    else {
        return empty;
    }
}

void DownloadUrisModel::
onUrisChanged()
{
    this->endResetModel();
}

} // namespace aria2_remote
