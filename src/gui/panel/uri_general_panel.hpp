#ifndef ARIA2_REMOTE_URI_GENERAL_PANEL_HPP
#define ARIA2_REMOTE_URI_GENERAL_PANEL_HPP

#include "utility/pointer.hpp"

#include <QWidget>

class QLabel;
class QFormLayout;

namespace aria2_remote {

class Aria2Remote;
class DownloadHandle;
class PieceWidget;

class UriGeneralPanel : public QWidget
{
    Q_OBJECT
public:
    UriGeneralPanel(QWidget& parent);

private:
    QLabel* addRow(QString const& name);

    Q_SLOT void setDownloadHandle(DownloadHandle const*);
    Q_SLOT void refresh();
    Q_SLOT void onHandleDestroyed();

private:
    DownloadHandle const* m_handle = nullptr;

    qmanaged_ptr<QLabel> m_gid;
    qmanaged_ptr<QLabel> m_status;
    qmanaged_ptr<QLabel> m_dir;
    qmanaged_ptr<QLabel> m_totalLength;
    qmanaged_ptr<QLabel> m_completedLength;
    qmanaged_ptr<QLabel> m_connections;
    qmanaged_ptr<QLabel> m_downloadSpeed;
    qmanaged_ptr<QLabel> m_pieceLength;
    qmanaged_ptr<QLabel> m_numPieces;
    qmanaged_ptr<PieceWidget> m_bitField;
    qmanaged_ptr<QLabel> m_errorMessage;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_URI_GENERAL_PANEL_HPP
