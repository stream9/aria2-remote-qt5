#ifndef ARIA2_REMOTE_TORRENT_GENERAL_PANEL_HPP
#define ARIA2_REMOTE_TORRENT_GENERAL_PANEL_HPP

#include "utility/pointer.hpp"

#include <QScrollArea>

class QCheckBox;
class QLabel;
class QTreeWidget;

namespace aria2_remote {

class DownloadHandle;
class PieceWidget;

class TorrentGeneralPanel : public QScrollArea
{
    Q_OBJECT
public:
    TorrentGeneralPanel(QWidget& parent);

private:
    Q_SLOT void setDownloadHandle(DownloadHandle const*);
    Q_SLOT void refresh();

private:
    DownloadHandle const* m_handle = nullptr;

    qmanaged_ptr<QLabel> m_gid;
    qmanaged_ptr<QLabel> m_name;
    qmanaged_ptr<QLabel> m_infoHash;
    qmanaged_ptr<QLabel> m_mode;
    qmanaged_ptr<QLabel> m_dir;
    qmanaged_ptr<QLabel> m_creationDate;
    qmanaged_ptr<QLabel> m_comment;

    qmanaged_ptr<QLabel> m_status;
    qmanaged_ptr<QLabel> m_length;
    qmanaged_ptr<QLabel> m_nodes;
    qmanaged_ptr<QCheckBox> m_seeding;
    qmanaged_ptr<QLabel> m_speed;
    qmanaged_ptr<QLabel> m_piece;
    qmanaged_ptr<PieceWidget> m_bitField;

    qmanaged_ptr<QTreeWidget> m_announceList;

    qmanaged_ptr<QLabel> m_errorMessage;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_TORRENT_GENERAL_PANEL_HPP
