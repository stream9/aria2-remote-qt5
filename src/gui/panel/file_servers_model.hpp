#ifndef ARIA2_REMOTE_FILE_SERVERS_MODEL_HPP
#define ARIA2_REMOTE_FILE_SERVERS_MODEL_HPP

#include <QAbstractItemModel>

class QModelIndex;
class QVariant;

namespace aria2_remote {

class DownloadHandle;
class FileData;
class ServerDatas;

class FileServersModel : public QAbstractItemModel
{
    enum Column { Uri = 0, CurrentUri, DownloadSpeed, Count };
public:
    FileServersModel() = default;
    FileServersModel(DownloadHandle const&, FileData const&);

    // override QAbstractItemModel
    QModelIndex index(int row, int column,
                      QModelIndex const& parent = {}) const override;
    QModelIndex parent(QModelIndex const&) const override;
    int rowCount(QModelIndex const& parent = {}) const override;
    int columnCount(QModelIndex const& parent = {}) const override;
    QVariant data(QModelIndex const&, int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation, int role = Qt::DisplayRole) const override;

private:
    ServerDatas const& servers() const;
    Q_SLOT void onDataChanged();

private:
    DownloadHandle const* m_handle = nullptr;
    FileData const* m_fileData = nullptr;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_FILE_SERVERS_MODEL_HPP
