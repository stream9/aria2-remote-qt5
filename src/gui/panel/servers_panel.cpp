#include "servers_panel.hpp"

#include "core/application.hpp"
#include "core/server.hpp"
#include "core/settings.hpp"
#include "gui/action_manager.hpp"

#include <QContextMenuEvent>
#include <QBoxLayout>
#include <QListWidget>
#include <QMenu>
#include <QToolButton>
#include <QToolBar>

namespace aria2_remote {

ServersPanel::
ServersPanel()
    : m_servers { make_qmanaged<QListWidget>(this) }
{
    auto layout = make_qmanaged<QVBoxLayout>(this);

    layout->addWidget(m_servers.get());

    auto& servers = gApp().servers();
    for (auto const& server: servers) {
        m_servers->addItem(server.name());
    }

    this->connect(&servers, &Servers::appended,
                  this,     &ServersPanel::onServerAppended);

    this->connect(&servers, &Servers::removed,
                  this,     &ServersPanel::onServerRemoved);

    this->connect(&servers, &Servers::edited,
                  this,     &ServersPanel::onServerEdited);

    this->connect(m_servers.get(), &QListWidget::itemSelectionChanged,
                  this,            &ServersPanel::onSelectionChanged);

    this->setFocusProxy(m_servers.get());
}

void ServersPanel::
readSettings()
{
    auto& settings = gApp().settings();

    settings.beginGroup("ServerPanel");

    auto const& name = settings.value("selection").toString();

    auto const& items = m_servers->findItems(name, Qt::MatchExactly);
    if (!items.empty()) {
        m_servers->setCurrentItem(items[0]);
    }

    settings.endGroup();
}

void ServersPanel::
writeSettings()
{
    auto& settings = gApp().settings();

    settings.beginGroup("ServerPanel");

    auto* const current = m_servers->currentItem();
    if (current) {
        settings.setValue("selection", current->text());
    }

    settings.endGroup();
}

void ServersPanel::
contextMenuEvent(QContextMenuEvent* const ev)
{
    auto& event = to_ref(ev);
    auto& actionManager = gApp().actionManager();

    QMenu menu;
    menu.addAction(&actionManager.addServerAction());
    menu.addAction(&actionManager.editServerAction());
    menu.addAction(&actionManager.removeServerAction());
    menu.addAction(&actionManager.connectAction());

    menu.exec(event.globalPos());
}

void ServersPanel::
keyPressEvent(QKeyEvent* const ev)
{
    auto& event = to_ref(ev);

    if (event.key() == Qt::Key_Return) {
        auto& actionManager = gApp().actionManager();

        actionManager.connectAction().trigger();

        Q_EMIT closeRequest();
    }
}

void ServersPanel::
onServerAppended()
{
    auto& servers = gApp().servers();
    assert(!servers.empty());

    auto& server = servers[servers.size()-1];

    m_servers->addItem(server.name());
}

void ServersPanel::
onServerRemoved(int const index)
{
    m_servers->takeItem(index);
}

void ServersPanel::
onServerEdited(int const index)
{
    assert(index >= 0);

    auto& servers = gApp().servers();

    auto& item = to_ref(m_servers->item(index));
    item.setText(servers[static_cast<size_t>(index)].name());
}

void ServersPanel::
onSelectionChanged()
{
    auto const& selection = m_servers->selectedItems();
    auto const size = selection.size();
    if (size == 0) {
        gApp().clearCurrentServer();
    }
    else if (size == 1) {
        auto const& item = to_ref(selection.front());
        auto const row = m_servers->row(&item);
        auto& servers = gApp().servers();

        gApp().setCurrentServer(servers[static_cast<size_t>(row)]);
    }
    else {
        assert(false);
    }
}

} // namespace aria2_remote
