#ifndef ARIA2_REMOTE_SERVERS_PANEL_HPP
#define ARIA2_REMOTE_SERVERS_PANEL_HPP

#include "utility/pointer.hpp"

#include <QWidget>

class QListWidget;
class QContextMenuEvent;

namespace aria2_remote {

class ServersPanel : public QWidget
{
    Q_OBJECT
public:
    ServersPanel();

    void readSettings();
    void writeSettings();

    Q_SIGNAL void closeRequest();

protected:
    // override QWidget
    void contextMenuEvent(QContextMenuEvent*) override;
    void keyPressEvent(QKeyEvent*) override;

    Q_SLOT void onServerAppended();
    Q_SLOT void onServerRemoved(int index);
    Q_SLOT void onServerEdited(int index);
    Q_SLOT void onSelectionChanged();

private:
    qmanaged_ptr<QListWidget> m_servers;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_SERVERS_PANEL_HPP
