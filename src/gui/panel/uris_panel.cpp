#include "uris_panel.hpp"

#include "download_uris_model.hpp"

#include "core/application.hpp"
#include "core/download_handle.hpp"
#include "gui/tree_view.hpp"

#include <cassert>

#include <QBoxLayout>
#include <QTreeView>

#include <QDebug>

namespace aria2_remote {

UrisPanel::
UrisPanel()
    : m_uris { make_qmanaged<TreeView>() }
    , m_model { std::make_unique<DownloadUrisModel>() }
{
    assert(m_model);

    auto layout = make_qmanaged<QVBoxLayout>(this);

    m_uris->setObjectName("UrisPanel TreeView");
    m_uris->setModel(m_model.get());

    layout->addWidget(m_uris.get());

    this->connect(&gApp(), &Application::currentHandleChanged,
                  this,    &UrisPanel::setDownloadHandle);
}

UrisPanel::~UrisPanel() = default;

void UrisPanel::
readSettings()
{
    m_uris->readSettings();
}

void UrisPanel::
writeSettings() const
{
    m_uris->writeSettings();
}

void UrisPanel::
setDownloadHandle(DownloadHandle* const handle)
{
    if (handle == m_handle) return;

    m_handle = handle;

    m_model = std::make_unique<DownloadUrisModel>(handle);
    assert(m_model);

    m_uris->setModel(m_model.get());
}

} // namespace aria2_remote
