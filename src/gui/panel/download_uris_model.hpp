#ifndef ARIA2_REMOTE_DOWNLOAD_URIS_MODEL_HPP
#define ARIA2_REMOTE_DOWNLOAD_URIS_MODEL_HPP

#include "uris_model.hpp"

namespace aria2_remote {

class DownloadHandle;

class DownloadUrisModel : public UrisModel
{
    Q_OBJECT
public:
    DownloadUrisModel(DownloadHandle* = nullptr);

    ~DownloadUrisModel();

private:
    UriDatas const& uris() const override;

    Q_SLOT void onUrisChanged();
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_DOWNLOAD_URIS_MODEL_HPP
