#include "file_uris_model.hpp"

#include "core/download_handle.hpp"
#include "core/file_data.hpp"

namespace aria2_remote {

FileUrisModel::
FileUrisModel()
    : UrisModel { nullptr }
{}

FileUrisModel::
FileUrisModel(DownloadHandle& handle, FileData const& fileData)
    : UrisModel { &handle }
    , m_fileData { &fileData }
{
    this->connect(&handle, &QObject::destroyed,
                  this,    &FileUrisModel::resetFile);
}

UrisModel::UriDatas const& FileUrisModel::
uris() const
{
    static UriDatas const empty;

    if (m_fileData) {
        return m_fileData->uris();
    }
    else {
        return empty;
    }
}

void FileUrisModel::
resetFile()
{
    m_fileData = nullptr;

    this->endResetModel();
}

} // namespace aria2_remote
