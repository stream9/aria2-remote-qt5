#include "header_context_menu.hpp"

#include "utility/pointer.hpp"

#include <cassert>

#include <QAbstractItemModel>
#include <QAction>
#include <QHeaderView>

namespace aria2_remote {

// HeaderContextMenu

HeaderContextMenu::
HeaderContextMenu(QHeaderView& header,
                  QAbstractItemModel const& model)
    : m_header { header }
{
    for (auto i = 0, len = model.columnCount(); i < len; ++i) {
        auto const& name =
                model.headerData(i, Qt::Horizontal).toString();

        auto& action = to_ref(this->addAction(name));
        action.setCheckable(true);
        action.setChecked(!header.isSectionHidden(i));
        action.setData(i);

        this->connect(&action, &QAction::toggled,
                      this,    &HeaderContextMenu::onToggled);
    }
}

void HeaderContextMenu::
onToggled(bool const checked)
{
    auto const& action = to_ref(dynamic_cast<QAction*>(this->sender()));
    auto const index = action.data().toInt();

    m_header.setSectionHidden(index, !checked);
}

} // namespace aria2_remote
