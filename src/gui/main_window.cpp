#include "main_window.hpp"

#include "action_manager.hpp"
#include "dialog/option_dialog.hpp"
#include "downloads_view.hpp"
#include "panel/files_panel.hpp"
#include "panel/general_panel.hpp"
#include "panel/peers_panel.hpp"
#include "panel/servers_panel.hpp"
#include "panel/uris_panel.hpp"
#include "status_bar.hpp"
#include "tool_bar/general_tool_bar.hpp"
#include "tool_bar/option_tool_bar.hpp"
#include "tool_bar/server_tool_bar.hpp"

#include "core/application.hpp"
#include "core/aria2_remote.hpp"
#include "core/command_line.hpp"
#include "core/options.hpp"
#include "core/server.hpp"
#include "core/settings.hpp"

#include <cassert>

#include <QCoreApplication>
#include <QUrl>
#include <QAction>
#include <QDockWidget>
#include <QMenu>
#include <QMenuBar>
#include <QToolBar>

namespace aria2_remote {

static QDockWidget&
createDock(QString const& label, QString const& name, QWidget& widget)
{
    auto* const dock = new QDockWidget { label };
    assert(dock);

    dock->setObjectName(name);
    dock->setWidget(&widget);
    assert(widget.parent());

    return *dock;
}

MainWindow::
MainWindow()
    : m_downloadsView { make_qmanaged<DownloadsView>() }
    , m_generalPanel { make_qmanaged<GeneralPanel>() }
    , m_filesPanel { make_qmanaged<FilesPanel>() }
    , m_urisPanel { make_qmanaged<UrisPanel>() }
    , m_serversPanel { make_qmanaged<ServersPanel>() }
    , m_peersPanel { make_qmanaged<PeersPanel>() }
    , m_statusBar { make_qmanaged<StatusBar>() }
{
    gApp().setMainWindow(*this);

    this->setTabPosition(Qt::BottomDockWidgetArea, QTabWidget::West);

    this->setCentralWidget(m_downloadsView);

    auto& generalDock = createDock("General", "GeneralPanel", *m_generalPanel);
    this->addDockWidget(Qt::BottomDockWidgetArea, &generalDock);
    assert(generalDock.parent());

    auto& filesDock = createDock("Files", "FilesPanel", *m_filesPanel);
    this->addDockWidget(Qt::BottomDockWidgetArea, &filesDock);
    assert(filesDock.parent());

    auto& urisDock = createDock("URIs", "UrisPanel", *m_urisPanel);
    this->addDockWidget(Qt::BottomDockWidgetArea, &urisDock);
    assert(urisDock.parent());

    auto& peersDock = createDock("Peers", "PeersPanel", *m_peersPanel);
    this->addDockWidget(Qt::BottomDockWidgetArea, &peersDock);
    assert(peersDock.parent());

    auto& serversDock = createDock("Servers", "ServersPanel", *m_serversPanel);
    this->addDockWidget(Qt::LeftDockWidgetArea, &serversDock);

    auto& toggle = to_ref(serversDock.toggleViewAction());
    toggle.setShortcut(Qt::Key_F1);

    this->connect(m_serversPanel, &ServersPanel::closeRequest,
                  &serversDock, &QWidget::hide);

    assert(serversDock.parent());

    this->setStatusBar(m_statusBar.get());

    createMenuBar();
    createToolBar();

    readSettings();

    this->connect(&gApp(), &Application::aria2Changed,
                  this,    &MainWindow::onAria2Changed);

    this->connect(qApp, &QCoreApplication::aboutToQuit,
                  this, &MainWindow::writeSettings);


    auto const& cmdLine = gCommandLine();
    if (cmdLine.server().isEmpty()) {
        serversDock.setVisible(true);
        m_serversPanel->setFocus();
    }
    else {
        serversDock.setVisible(false);
        gApp().connectToServer(cmdLine.server());
    }
}

MainWindow::~MainWindow() = default;

QSize MainWindow::
sizeHint() const
{
    return { 400, 200 };
}

void MainWindow::
closeEvent(QCloseEvent*)
{
    writeSettings();
}

void MainWindow::
readSettings()
{
    auto& settings = gApp().settings();

    settings.beginGroup("MainWindow");

    this->restoreGeometry(settings.value("geometry").toByteArray());
    this->restoreState(settings.value("state").toByteArray());

    settings.endGroup();

    m_downloadsView->readSettings();
    m_filesPanel->readSettings();
    m_urisPanel->readSettings();
    m_serversPanel->readSettings();

    gApp().readSettings();
}

void MainWindow::
writeSettings()
{
    auto& settings = gApp().settings();

    settings.beginGroup("MainWindow");

    settings.setValue("geometry", this->saveGeometry());
    settings.setValue("state", this->saveState());

    settings.endGroup();

    m_downloadsView->writeSettings();
    m_filesPanel->writeSettings();
    m_urisPanel->writeSettings();
    m_serversPanel->writeSettings();

    gApp().writeSettings();
}

void MainWindow::
createMenuBar()
{
    createFileMenu();
    createViewMenu();
}

void MainWindow::
createFileMenu()
{
    auto& menuBar = to_ref(this->menuBar());

    auto& menu = to_ref(menuBar.addMenu("&File"));
    assert(menu.parent());

    menu.addAction(&gApp().actionManager().quitAction());
}

void MainWindow::
createViewMenu()
{
    auto& menuBar = to_ref(this->menuBar());

    auto& menu = to_ref(menuBar.addMenu("&View"));
    assert(menu.parent());

    auto& dockMenu = to_ref(menu.addMenu("&Docks"));
    assert(dockMenu.parent());

    auto const& docks = this->findChildren<QDockWidget*>();
    for (auto* const dock: docks) {
        assert(dock);
        dockMenu.addAction(dock->toggleViewAction());
    }

    menu.addAction(&gApp().actionManager().optionAction());
}

void MainWindow::
createToolBar()
{
    auto general = make_qmanaged<GeneralToolBar>(gApp().actionManager());
    this->addToolBar(general.get());

    auto option = make_qmanaged<OptionToolBar>(gApp());
    this->addToolBar(option.get());

    auto server = make_qmanaged<ServerToolBar>(gApp().actionManager());
    this->addToolBar(server.get());
}

void MainWindow::
onAria2Changed(Aria2Remote* const aria2)
{
    auto const& appName = qApp->applicationName();

    if (aria2) {
        this->setWindowTitle(appName + " - " + aria2->server().name());
    }
    else {
        this->setWindowTitle(appName);
    }
}

} // namespace aria2_remote
