#ifndef ARIA2_REMOTE_DOWNLOADS_MODEL_HPP
#define ARIA2_REMOTE_DOWNLOADS_MODEL_HPP

#include <memory>

#include <QAbstractItemModel>

class QModelIndex;
class QVariant;

namespace aria2_remote {

class Aria2Remote;
class ConstValidModelIndex;
class DownloadHandle;

class DownloadsModel : public QAbstractItemModel
{
    Q_OBJECT

    enum Column {
        Name = 0, Status, Progress, TotalLength, CompletedLength,
        Eta, Connections, DownloadSpeed, PieceLength, NumPieces,
        Count
    };

public:
    DownloadsModel() = default;
    DownloadsModel(Aria2Remote&);

    // override QAbstractItemModel
    QModelIndex index(int row, int column,
                      QModelIndex const& parent = {}) const override;
    QModelIndex parent(QModelIndex const&) const override;
    int rowCount(QModelIndex const& parent = {}) const override;
    int columnCount(QModelIndex const& parent = {}) const override;
    QVariant data(QModelIndex const&, int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation, int role = Qt::DisplayRole) const override;
    Qt::ItemFlags flags(QModelIndex const&) const noexcept override;

private:
    DownloadHandle& getHandle(int row) const;
    DownloadHandle const& getHandle(ConstValidModelIndex const&) const;

    int getRow(DownloadHandle&) const;

    QVariant displayData(ConstValidModelIndex const&) const;
    QVariant toolTipData(ConstValidModelIndex const&) const;
    QVariant alignmentData(ConstValidModelIndex const&) const;

    void onDownloadCreated(size_t index);
    void onDownloadChanged(size_t index);
    void onDownloadRemoved(size_t index);
    void onDownloadMoved(size_t from, size_t to);

    Q_SLOT void onActiveDownloadCreated(size_t index, DownloadHandle&);
    Q_SLOT void onActiveDownloadChanged(size_t index, DownloadHandle&);
    Q_SLOT void onActiveDownloadRemoved(size_t index);
    Q_SLOT void onActiveDownloadMoved(size_t from, size_t to, DownloadHandle&);

    Q_SLOT void onWaitingDownloadCreated(size_t index, DownloadHandle&);
    Q_SLOT void onWaitingDownloadChanged(size_t index, DownloadHandle&);
    Q_SLOT void onWaitingDownloadRemoved(size_t index);
    Q_SLOT void onWaitingDownloadMoved(size_t from, size_t to, DownloadHandle&);

    Q_SLOT void onStoppedDownloadCreated(size_t index, DownloadHandle&);
    Q_SLOT void onStoppedDownloadChanged(size_t index, DownloadHandle&);
    Q_SLOT void onStoppedDownloadRemoved(size_t index);
    Q_SLOT void onStoppedDownloadMoved(size_t from, size_t to, DownloadHandle&);

private:
    std::shared_ptr<Aria2Remote> m_aria2; // nullable
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_DOWNLOADS_MODEL_HPP
